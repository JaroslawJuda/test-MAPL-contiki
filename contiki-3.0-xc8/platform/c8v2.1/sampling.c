#include "contiki.h"
#include "sampling.h"
#include "dev/ports-adc.h"
#include "dev/ports-leds.h"
#include "sys/process.h"

static void init(void);
static void stop(void);

static struct process* observer_process = NULL;
process_event_t sampling_event;

volatile uint8_t sampling_adc_buffer[SAMPLING_ADC_SAMPLES_NBR] = {0};
volatile uint8_t sampling_adc_writing_pointer;
static uint8_t sampling_adc_reading_pointer;


uint8_t sampling_output_buffer[UINT8_MAX + 1] = {0};
uint8_t sampling_writing_pointer;
uint8_t sampling_reading_pointer;

//Wartość komparatora, przy przetwarzaniu wartości analogowej na zerojedynkową
#define THRESHOLD (128u)
#define HYSTERESIS (32u)

PROCESS(sampling_process, "Sampling ports process");

PROCESS_THREAD(sampling_process, ev, data) {
    static uint8_t bit_position;
    PROCESS_BEGIN();
    init();
    while (1) {
        PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_POLL);
        //pilnujemy, żeby odczyt nie następował szybciej niż zapis,
        //sprawdzamy, czy odległość między wskaźnikami w buforze próbek
        //nie zmniejszyła się do zera, ma być zawsze co najmniej jeden
        while (sampling_adc_writing_pointer != sampling_adc_reading_pointer) {
            bit_position = sampling_adc_reading_pointer & (SAMPLING_CHANNELS_NBR - 1u);
            if (sampling_adc_buffer[sampling_adc_reading_pointer]>(THRESHOLD)) {
                sampling_output_buffer[sampling_writing_pointer] |= (1u << bit_position);
            } else {
                sampling_output_buffer[sampling_writing_pointer] &= ~(1u << bit_position);
            }
            sampling_adc_reading_pointer++;
            if (sampling_adc_reading_pointer % SAMPLING_CHANNELS_NBR == 0) {
                ports_leds_set(~sampling_output_buffer[sampling_writing_pointer]);
                if (process_is_running(observer_process)) {
                    process_post_synch(observer_process, sampling_event, NULL);
                    PROCESS_YIELD();
                }
                sampling_writing_pointer++;
            }
        }
        //PROCESS_WAIT_UNTIL(ev == PROCESS_EVENT_EXIT);
    }
    PROCESS_END();
}

static void init(void) {

    ports_adc_init();
    TRISC0 = 0;
    TRISC1 = 1;

    sampling_adc_writing_pointer = -1;
    sampling_writing_pointer = 0;
    sampling_reading_pointer = 0;

    T3CON = 0b10110000u; //Fosc/4, preskaler 1:8
    T3CONbits.TMR3ON = 1;
    PIR2bits.TMR3IF = 0;
    IPR2bits.TMR3IP = 1; //Timer3 high priority interrupt
    PIE2bits.TMR3IE = 1;
    sampling_event = process_alloc_event();
}

void sampling_register_observer(struct process* observer) {
    observer_process = observer;
}
