#include "contiki.h"
#include "sampling.h"
#include "puls-sync.h"

static volatile uint16_t tmp;

//Capture mode; every falling edgeCCP
#define  CCPCON_FALLING (0b00000100u)

//Capture mode; every rising edge
#define  CCPCON_RISING (0b00000101u)

#define CCPCON_RESET (0u)

extern volatile DWORD dwInternalTicks;

void interrupt high_isr(void) {
    //Próbkowanie portow
    if (PIR2bits.TMR3IF && PIE2bits.TMR3IE) {
#if (GetSystemClock()==(41666667ul))
        tmp = 62281u + TMR3L; //przerwanie 400 razy na sekunde
        tmp += (TMR3H << 8);
#else
#error Dopasuj czestotliwosc probkowania do zegara
#endif

#if(SAMPLING_CHANNELS_NBR!=8)
#error Ten kod jest napisany dla ośmiu kanałów
#endif
        TMR3H = *(((uint8_t*) (&tmp)) + 1);
        TMR3L = (uint8_t) tmp;

        sampling_adc_buffer[sampling_adc_writing_pointer++] = ADRESH;

        switch (sampling_adc_writing_pointer & 0x7u) {
            case 0:
                ADCON0bits.CHS = 2u;
                break;
            case 1:
                ADCON0bits.CHS = 3u;
                break;
            default:
                ADCON0bits.CHS = 4u + (sampling_adc_writing_pointer & 0x7u);
        }

        ADCON0bits.ADON = 1;
        ADCON0bits.GO = 1;
        if ((sampling_adc_writing_pointer & 0x7u) == 0) {
            process_poll(&sampling_process);
        }

        LATC0 = !PORTCbits.RC1;
        
        PIR2bits.TMR3IF = 0;
    }
}
// NOTE: Several PICs, including the PIC18F4620 revision A3 have a RETFIE FAST/MOVFF bug
// The interruptlow keyword is used to work around the bug when using C18

void interrupt low_priority low_isr(void) {

    if (PIR2bits.CCP2IF && PIE2bits.CCP2IE) {
        process_poll(&pulse_sync_process);
        PIR2bits.CCP2IF = 0;
    }
    //Aktualizacja zegara contiki
    if (INTCONbits.TMR0IF && INTCONbits.TMR0IE) {
        dwInternalTicks++;
        INTCONbits.TMR0IF = 0;
    }

    if (PIR1bits.TMR2IF && PIE1bits.TMR2IE) {

        PIR1bits.TMR2IF = 0;
    }
}


