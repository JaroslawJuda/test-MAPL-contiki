#ifndef PORTS_SHIFTER_H_
#define PORTS_SHIFTER_H_

#define BLINK TRUE
#define NO_BLINK FALSE

void ports_shifter_set(uint16_t pattern);
void ports_shifter_shift(uint8_t pattern);
void ports_shifter_init(void);
#endif //#ifndef PORTS_SHIFTER_H_
