#ifndef _ports_adc_h_
#define _ports_adc_h_

#include "contiki-conf.h"

void ports_adc_init(void);

unsigned char ports_adc_get_voltage(unsigned char channel);

#endif

