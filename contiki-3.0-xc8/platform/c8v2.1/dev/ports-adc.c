#include "contiki.h"
#include "dev/ports-adc.h"

static void init(void) {
    ADCON0 = 0u;
    ADCON1 = 0u; // wykorzystane wszystkie porty analogowe
    ADCON2 = 0b00111110u;

    PIR1bits.ADIF = 0;
    PIE1bits.ADIE = 0; //wyłączamy przerwanie od ADC

    // ustawiamy port
    LATA &= ~0b00001100u;
    TRISA |= 0b00001100u;
    LATF &= ~0b01111110u;
    TRISF |= 0b01111110u;

    ADCON0bits.ADCAL = 1u; //wykonujemy kalibrację 
    ADCON0bits.ADON = 1;
    ADCON0bits.GO = 1;
    Nop();
    while (ADCON0bits.GO);

    ADCON0bits.ADCAL = 0u;

}

static void start(void) {
    ADCON0bits.ADON = 1;
    PIR1bits.ADIF = 0;
    PIE1bits.ADIE = 0;
    ADCON0bits.GO = 1;
}

static void set_mux(uint8_t channel) {
    if (channel >= 8) return;
    switch (channel) {
        case 0:
            ADCON0bits.CHS = 2u;
            break;
        case 1:
            ADCON0bits.CHS = 3u;
            break;
        default:
            ADCON0bits.CHS = channel + 4u;
    }
}

void ports_adc_init(void) {
    init();
}

/*!
 * unsigned char ports_adc_get_voltage(unsigned char channel)
 * \param channel numer kanalu, na ktorym chcemy wykonac pomiar
 * \return napiecie na aktualnie mierzonym kanale wyrazone w jednostkach przetwornika
 *
 * Wykonanie pomiaru napiecia na podanym kanale.
 */
unsigned char ports_adc_get_voltage(unsigned char channel) {
    set_mux(channel); // ustawienie kanalu do pomiaru
    start();
    while (ADCON0bits.NOT_DONE); // oczekiwanie na zakonczenie pomiaru

    return ADRESH; // podanie wyniku pomiaru na 8 bitach
}
