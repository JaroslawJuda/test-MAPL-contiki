#include "contiki-conf.h"
#include "dev/ports-shifter.h"


#define SHIFTER_CLOCK           LATCbits.LATC3
#define SHIFTER_DATA_1          LATCbits.LATC5
#define SHIFTER_DATA_2          LATCbits.LATC4
#define SHIFTER_STROBE          LATBbits.LATB2

#define SHIFTER_CLOCK_TRIS      TRISCbits.TRISC3
#define SHIFTER_DATA_1_TRIS     TRISCbits.TRISC5
#define SHIFTER_DATA_2_TRIS     TRISCbits.TRISC4
#define SHIFTER_STROBE_TRIS     TRISBbits.TRISB2

#define SHIFTER_INIT() {LATC &= 0b11000111; TRISC &= 0b11000111; SHIFTER_STROBE=0; SHIFTER_STROBE_TRIS=0; SPI_ON_BIT=0;}

static void output_all(void) {
    SHIFTER_STROBE = 1;
    Nop();
    SHIFTER_STROBE = 0;
    Nop();
    SHIFTER_STROBE = 1;
}

static void init(void);

static uint16_t leds_pattern = 0ul;
static uint16_t blink_leds_pattern = 0ul;

static void init(void) {
    SHIFTER_INIT();
}

void ports_shifter_set(uint16_t pattern) {
    uint8_t i;
    SHIFTER_INIT();
    i = 8;
    do {
        SHIFTER_DATA_1 = (pattern & 0b1ul) ? 1 : 0;
        SHIFTER_DATA_2 = (pattern & 0b100000000ul) ? 1 : 0;

        pattern >>= 1;

        SHIFTER_CLOCK = 0;
        Nop();
        SHIFTER_CLOCK = 1;
    } while (--i);
    output_all();
    SHIFTER_DATA_1 = 1;
    SHIFTER_DATA_2 = 1;
}

void ports_shifter_init() {
    init();
}


//! Przesunięcie zawartości rejestrów o jeden port i wprowadzenie zawartości na najmłodsze bity

void ports_shifter_shift(uint8_t pattern) {
    uint8_t i;
    SHIFTER_INIT();
    i = 2;
    do {
        SHIFTER_DATA_1 = (pattern & 0b1ul) ? 1 : 0;
        SHIFTER_DATA_2 = (pattern & 0b100ul) ? 1 : 0;

        pattern >>= 1;

        SHIFTER_CLOCK = 0;
        Nop();
        SHIFTER_CLOCK = 1;
    } while (--i);
    output_all();
    SHIFTER_DATA_1 = 1;
    SHIFTER_DATA_2 = 1;
}
