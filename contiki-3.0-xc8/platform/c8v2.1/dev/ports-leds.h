#ifndef PORTS_LEDS_H_
#define PORTS_LEDS_H_

#include "contiki.h"
/*!
* Kolory ledow dla modulu ledowego dla kabelkow i portow.
*/
typedef  enum _PortsLedsColor {
	PORTS_LEDS_COLOR_GREEN = 0, PORTS_LEDS_COLOR_RED, PORTS_LEDS_COLOR_COLORS_NUMBER
} PortsLedsColor;



void ports_leds_init(void);
PT_THREAD(ports_leds_task(struct pt* pt));
bool ports_leds_is_busy(void);
void ports_leds_on(unsigned char led, PortsLedsColor);
void ports_leds_off(unsigned char led, PortsLedsColor);
void ports_leds_blink(unsigned char led, PortsLedsColor);
void ports_leds_flash_once(unsigned char led, PortsLedsColor);
void ports_leds_sync_blink(void);

/**
 * Funkcja ustawia stan podświetlenia portów
 * @param mask Maska bitowa podświetlenia
 */
void ports_leds_set(uint8_t mask);


/*!
* Przerwa pomiedzy wywolaniem modulu w ms
*/
	#define PORTS_LEDS_TICK_PERIOD			(50u)


PROCESS_NAME(ports_leds_process);

#endif
