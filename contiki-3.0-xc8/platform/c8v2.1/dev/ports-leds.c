#include "contiki.h"
#include "sys/pt.h"
#include "dev/ports-leds.h"
#include "sys/timer.h"

#define LEDS_PORTS_NUMBER (8u)
static int8_t blink = 0;
static struct pt child_pt;

PROCESS(ports_leds_process, "Podswietlenie portow");

struct etimer ports_leds_timer;

PROCESS_THREAD(ports_leds_process, ev, data) {

    PROCESS_BEGIN();
    ports_leds_timer.next = _OMNITARGET;
    ports_leds_init();
    while (1) {
        etimer_set(&ports_leds_timer, CLOCK_SECOND / 10ul);
        PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_TIMER || ev== PROCESS_EVENT_POLL);
        PT_INIT(&child_pt);
        while (PT_SCHEDULE(ports_leds_task(&child_pt))) {
            Nop();
        }
    }
    PROCESS_END();
}


/*!
 * @name Definicje wyprowadzen uC dla modulu ledowego dla kabelkow i portow.
 */
//@{

// SDI
#define PORTS_LEDS_SDI_IO	(LATGbits.LATG4)
#define PORTS_LEDS_SDI_TRIS	(TRISGbits.TRISG4)

// LA
#define PORTS_LEDS_LA_IO	(LATEbits.LATE1)
#define PORTS_LEDS_LA_TRIS	(TRISEbits.TRISE1)

// CLK
#define PORTS_LEDS_CLK_IO	(LATEbits.LATE0)
#define PORTS_LEDS_CLK_TRIS	(TRISEbits.TRISE0)

// Color Select
#define PORTS_LEDS_COLOR_SELECT_IO	(LATB)
#define PORTS_LEDS_COLOR_SELECT_TRIS	(TRISB)
#define PORTS_LEDS_COLOR_SELECT_MASK    (0b00000011u)

//@}


static uint8_t leds_vector_permanent[PORTS_LEDS_COLOR_COLORS_NUMBER];
static uint8_t leds_vector_blinking[PORTS_LEDS_COLOR_COLORS_NUMBER];
static uint8_t leds_vector_flashing_once[PORTS_LEDS_COLOR_COLORS_NUMBER];

static void send_out_vector(uint8_t vector);

/*!
 * void ports_leds_init(void)
 *
 * Inicjalizacja modulu ledowego dla kabelkow i portow.
 * \n\n Ustawienia portow.
 */
void ports_leds_init(void) {

    // poczatkowe ustawienie portow
    PORTS_LEDS_CLK_IO = 0;
    PORTS_LEDS_CLK_TRIS = 0;

    PORTS_LEDS_LA_IO = 0;
    PORTS_LEDS_LA_TRIS = 0;

    PORTS_LEDS_SDI_IO = 0;
    PORTS_LEDS_SDI_TRIS = 0;

    PORTS_LEDS_COLOR_SELECT_TRIS &= ~PORTS_LEDS_COLOR_SELECT_MASK;
    PORTS_LEDS_COLOR_SELECT_IO &= ~PORTS_LEDS_COLOR_SELECT_MASK;

    memset(leds_vector_permanent, 0, sizeof (leds_vector_permanent));
    memset(leds_vector_blinking, 0, sizeof (leds_vector_blinking));
}

/*!
 * //!function  ports_leds_tick(void)
 *
 * okresowe odpalanie procedury obslugujacej diody
 */
PT_THREAD(ports_leds_task(struct pt* pt)) {
    PT_BEGIN(pt);
    PORTS_LEDS_CLK_TRIS = 0;
    PORTS_LEDS_LA_TRIS = 0;
    PORTS_LEDS_SDI_TRIS = 0;
    PORTS_LEDS_COLOR_SELECT_TRIS &= ~PORTS_LEDS_COLOR_SELECT_MASK;

    send_out_vector(~leds_vector_permanent[PORTS_LEDS_COLOR_GREEN]);
    PORTS_LEDS_COLOR_SELECT_IO |= PORTS_LEDS_COLOR_SELECT_MASK;


    PT_EXIT(pt);
    PT_END(pt);
}

void ports_leds_sync_blink(void) {

    blink = 0; //zaraz wszystko mrugnie
}

/*!
 * void ports_leds_on(unsigned char led, unsigned char mark)
 * \param led numer diody, ktora ma zostac wlaczona
 * \param mark kolor diody, ktora ma zostac wlaczona
 *
 * Wlaczenie jednej diody.
 */
void ports_leds_on(unsigned char led, PortsLedsColor color) {
    if (led >= LEDS_PORTS_NUMBER) return;
    if (color >= PORTS_LEDS_COLOR_COLORS_NUMBER) return;

    leds_vector_permanent[color] |= (0b1u << led);
    leds_vector_blinking[color] |= (0b1u << led);
}

/*!
 * void ports_leds_off(unsigned char led, unsigned char mark)
 * \param led numer diody, ktora ma zostac wylaczona
 * \param mark kolor diody, ktora ma zostac wylaczona
 *
 * Wylaczenie jednej diody.
 */
void ports_leds_off(unsigned char led, PortsLedsColor color) {
    if (led >= LEDS_PORTS_NUMBER) return;
    if (color >= PORTS_LEDS_COLOR_COLORS_NUMBER) return;

    leds_vector_permanent[color] &= ~(0b1u << led);
    leds_vector_blinking[color] &= ~(0b1u << led);
}

/*!
 * void ports_leds_blink(unsigned char led, unsigned char mark)
 * \param led numer diody, ktora ma byc migana
 * \param mark kolor diody, ktora ma byc migana
 *
 * Miganie jednej diody.
 */
void ports_leds_blink(unsigned char led, PortsLedsColor color) {
    if (led >= LEDS_PORTS_NUMBER) return;
    if (color >= PORTS_LEDS_COLOR_COLORS_NUMBER) return;

    leds_vector_blinking[color] |= (0b1u << led);
    leds_vector_permanent[color] &= ~(0b1u << led);

}

/*!
 * void ports_flash_once(unsigned char led, unsigned char mark)
 * \param led numer diody, ktora ma byc raz mignieta
 * \param mark kolor diody, ktora ma byc raz mignieta
 *
 * Pojedyncze migniecie jednej diody.
 */
void ports_leds_flash_once(unsigned char led, PortsLedsColor color) {
    if (led >= LEDS_PORTS_NUMBER) return;
    if (color >= PORTS_LEDS_COLOR_COLORS_NUMBER) return;

    leds_vector_flashing_once[color] |= (0b1u << led);
}


void ports_leds_set(uint8_t mask){
    leds_vector_permanent[PORTS_LEDS_COLOR_GREEN]=mask;
}

/*!
 * void send_out_vectors(bool blink)
 * \param blink parametr okreslajacy, czy w danym cyklu diody maja migac
 *
 * Funkcja tworzy wektor diod i podaje go na zewnatrz.
 */
static void send_out_vector(uint8_t vector) {
    uint8_t i;

    PORTS_LEDS_LA_IO = 0;

    for (i = 0; i < LEDS_PORTS_NUMBER; i++) {
        PORTS_LEDS_CLK_IO = 0;
        Nop();
        PORTS_LEDS_SDI_IO = (vector & 0b10000000ul) ? 1 : 0;
        vector <<= 1;
        PORTS_LEDS_CLK_IO = 1;
        Nop();
    }
    Nop();

    PORTS_LEDS_LA_IO = 1;
    Nop();
}
