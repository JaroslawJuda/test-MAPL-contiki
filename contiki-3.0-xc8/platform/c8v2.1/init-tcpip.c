#include "contiki-conf.h"
#include "TCPIPStack/TCPIP.h"

void init_TCPIP(void)
{
    // Initialize core stack layers (MAC, ARP, TCP, UDP)
    StackInit();


#if defined(STACK_USE_DHCP_CLIENT)
    if(!AppConfig.Flags.bIsDHCPEnabled)
    {
        DHCPDisable(0);
    }
#endif

}
