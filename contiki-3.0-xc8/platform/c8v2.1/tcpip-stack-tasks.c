#include "contiki-conf.h"
#include "TCPIPStack/TCPIP.h"
#include "app-config.h"



//static char TEMPString[8] = "27.1C";

#if !defined(STACK_USE_DHCP_CLIENT)
#define DHCPBindCount	(0xFF)
#endif


//void UDPReporterTask(void);
void ReporterTask(void);

void alive_task(void);

void tcpip_tasks(void) {
    static DWORD dwLastIP = 0;
    static DWORD dwLastMask = 0;

    // This task performs normal stack task including checking
    // for incoming packet, type of packet and calling
    // appropriate stack entity to process it.
    StackTask();

    //SNTPClient();

#if defined(STACK_USE_ANNOUNCE)
    DiscoveryTask();
#endif

#if defined(STACK_USE_HTTP_SERVER) || defined(STACK_USE_HTTP2_SERVER)
    HTTPServer();
#endif

#if defined(STACK_USE_NBNS)
    NBNSTask();
#endif

    // For DHCP information, display how many times we have renewed the IP
    // configuration since last reset.
    // If the local IP address has changed (ex: due to DHCP lease change)
    // write the new IP address to the LCD display, UART, and Announce
    // service
    if ((dwLastIP != AppConfig.MyIPAddr.Val) || (dwLastMask != AppConfig.MyMask.Val)) {
        dwLastIP = AppConfig.MyIPAddr.Val;
        dwLastMask = AppConfig.MyMask.Val;

        AppConfig.DefaultIPAddr = AppConfig.MyIPAddr;
        AppConfig.DefaultMask = AppConfig.MyMask;
        SaveAppConfig(&AppConfig);

#if defined(STACK_USE_ANNOUNCE)
        AnnounceIP();
#endif
    }
}
