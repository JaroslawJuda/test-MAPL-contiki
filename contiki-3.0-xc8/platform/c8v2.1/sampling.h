#ifndef SAMPLING_H_
#define SAMPLING_H_

#include "contiki.h"


#define SAMPLING_CHANNELS_NBR (8u)
#if(SAMPLING_CHANNELS_NBR!=8)
#error Dopasuj kod do innej liczby kanałów
#endif

#define SAMPLING_ADC_SAMPLES_NBR (UINT8_MAX+1)

#if(SAMPLING_ADC_SAMPLES_NBR>UINT8_MAX+1)
#error Za duży bufor próbek
#endif

//! bufor cykliczny pr�bek
extern volatile uint8_t sampling_adc_buffer[SAMPLING_ADC_SAMPLES_NBR];
extern volatile uint8_t sampling_adc_writing_pointer;

extern uint8_t sampling_output_buffer[UINT8_MAX + 1];
extern uint8_t sampling_writing_pointer;
extern uint8_t sampling_reading_pointer;

/**
 * Funkcja rejestruje proces zainteresowany otrzymywaniem danych.
 * Może byc tylko jeden proces.
 * @param observer Proces nasłuchujący
 */
void sampling_register_observer(struct process* observer);

extern process_event_t sampling_event;

PROCESS_NAME(sampling_process);


#endif //#ifndef SAMPLES_H_
