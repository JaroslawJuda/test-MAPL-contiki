#ifndef HARDWAREPROFILE_H_
#define HARDWAREPROFILE_H_


#include "Compiler.h"

// Clock frequency values
// These directly influence timed events using the Tick module.  They also are used for UART and SPI baud rate generation.


#define GetSystemClock()		(41666667ul)
//PLL enabled
// Czestotliwosc 25MHz PLLx5 Prescaler/3 Postscaler disabled
// Idle after Sleep
#define system_clock_init() {OSCCON=0x80; OSCTUNE=0b01000000u;}


#define GetInstructionClock()	(GetSystemClock()/4)	// Normally GetSystemClock()/4 for PIC18, GetSystemClock()/2 for PIC24/dsPIC, and GetSystemClock()/1 for PIC32.  Might need changing if using Doze modes.
#define GetPeripheralClock()	(GetSystemClock()/4)	// Normally GetSystemClock()/4 for PIC18, GetSystemClock()/2 for PIC24/dsPIC, and GetSystemClock()/1 for PIC32.  Divisor may be different if using a PIC32 since it's configurable.

static unsigned char dummy_LED;
// Hardware I/O pin mappings


#define SPIFLASH_CS_TRIS        TRISAbits.TRISA4
#define SPIFLASH_CS_IO          LATAbits.LATA4

#define SPIFLASH_SCK_TRIS    TRISCbits.TRISC3
#define SPIFLASH_SDI_TRIS    TRISCbits.TRISC4
#define SPIFLASH_SDO_TRIS    TRISCbits.TRISC5
#define SPIFLASH_SPICON1     SSP1CON1
#define SPIFLASH_SPICON1bits SSP1CON1bits
#define SPIFLASH_SSPBUF      SSP1BUF
#define SPIFLASH_SPISTAT     SSP1STAT
#define SPIFLASH_SPISTATbits SSP1STATbits
#define SPIFLASH_SPI_IF     PIR1bits.SSP1IF
#define SPI_ON_BIT   (SPIFLASH_SPICON1bits.SSPEN)

// UART mapping functions for consistent API names across 8-bit and 16 or
// 32 bit compilers.  For simplicity, everything will use "UART" instead
// of USART/EUSART/etc.
#define BusyUART()		BusyUSART()
#define CloseUART()		CloseUSART()
#define ConfigIntUART(a)	ConfigIntUSART(a)
#define DataRdyUART()		DataRdyUSART()
#define OpenUART(a,b,c)		OpenUSART(a,b,c)
#define ReadUART()		ReadUSART()
#define WriteUART(a)		WriteUSART(a)
#define getsUART(a,b,c)		getsUSART(b,a)
#define putsUART(a)		putsUSART(a)
#define putrsUART(a)		putrsUSART((far rom char*)a)

#endif
