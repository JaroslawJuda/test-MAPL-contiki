#ifndef CONTIKI_CONF_H_
#define CONTIKI_CONF_H_

#include <xc.h>
#include "Compiler.h"
#include <GenericTypeDefs.h>
#include <stdint.h>
#include <limits.h>
#include <stdbool.h>
#include "spi-flash-space-distrib.h"
#include "HardwareProfile.h"
#include "TCPIPConfig.h"


int strncasecmp(const char *str1, const char *str2, size_t n);


#define CCIF
#define CLIF
#define CC_CONF_REGISTER_ARGS          0
#define CC_CONF_FUNCTION_POINTER_ARGS  0
#define CC_CONF_NO_VA_ARGS             1
#define CC_CONF_INLINE                 inline

//#ifdef __XC8
//#define XC8_POINTER_CMP(ptr1,ptr2) (((uint16_t)(ptr1)==(uint16_t)(ptr2))?true:false) 
//#endif


#define PTR_BASE		unsigned short
#define ROM_PTR_BASE	unsigned long

typedef unsigned long clock_time_t;
typedef unsigned long rtimer_clock_t;

#ifndef TICKS_PER_SECOND
#define TICKS_PER_SECOND		((GetPeripheralClock()+128ull)/256ull)	// Internal core clock drives timer with 1:256 prescaler
#endif

#define CLOCK_CONF_SECOND          TICKS_PER_SECOND
#define STIMER_TICK_UPDATE_FREQ    ((GetPeripheralClock())>>16ull)
#define STIMER_TICK_CORRECTION     ((GetPeripheralClock()&0xffffull)>>8ull)

#if(STIMER_TICK_UPDATE_FREQ==0)
#error adjust stimer update frequency
#endif
#if(STIMER_TICK_UPDATE_FREQ>0xfful)
#error adjust stimer update frequency
#endif


#define CONTIKI_CONF_SETTINGS_MANAGER 0

#define RTIMER_CLOCK_LT(a,b) ((int16_t)((a)-(b))<0)

#define INFINITE_TIME ULONG_MAX

typedef unsigned short uip_stats_t;

#define UIP_CONF_IPV6 0
#define UIP_CONF_BUFFER_SIZE 128
#define UIP_CONF_IPV6_RPL 0
#define UIP_CONF_UDP_CONNS 2


#define MMEM_CONF_SIZE (200u)

#define PROCESS_CONF_NO_PROCESS_NAMES 1

#define QUEUEBUF_CONF_NUM (2u)
#define PACKETBUF_CONF_HDR_SIZE (24u)
#define UIP_CONF_MAX_LISTENPORTS (5u)
#define QUEUEBUF_CONF_REF_NUM (1u)
#define PACKETBUF_CONF_SIZE (64u)

//#define UIP_CONF_TCP 0



#define DISABLE_INTERRUPTS()    {INTCONbits.GIEL = 0; INTCONbits.GIEH = 0;}
#define ENABLE_INTERRUPTS()     {INTCONbits.GIEH = 1; INTCONbits.GIEL = 1;}

#define member_sizeof(T,F) sizeof(((T *)0)->F)


#define WITH_TCPIP 1

#endif /* __CONTIKI_CONF_H__ */

