#include "contiki-conf.h"

void init_base_hardware(void) {

    //wyłączenie SPI
    RCSTAbits.SPEN = 0;
    SSPCON1bits.SSPEN = 0;

    //wyłączenie PWM i CCP
    CCP1CONbits.CCP1M3 = 0;
    CCP1CONbits.CCP1M2 = 0;
    CCP1CONbits.CCP1M1 = 0;
    CCP1CONbits.CCP1M0 = 0;

    RCONbits.IPEN = 1; // Enable interrupt priorities
    INTCONbits.GIEH = 1;
    INTCONbits.GIEL = 1;

    system_clock_init();

}
