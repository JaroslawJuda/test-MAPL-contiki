#ifndef DEVICE_CONF_H_
#define DEVICE_CONF_H_

#include "contiki-conf.h"

#define DEVICE_ID_SERIAL_LENGTH (6u)

struct DeviceConf {
    uint8_t reserved ;
};


extern const rom uint32_t device_conf_ID;
extern const rom struct DeviceConf device_conf;

/**
 * Zwraca identyfikator urządzenia w formacie tekstowym szestnastkowym.
 * Sześć znaków + znak '\0'
 * @param buf Wskaźnik na bufor wyjściowy
 * @return Wskaźnik na koniec bufora po wypełnieniu, zawiera znak '\0'
 */
char *
device_conf_get_id_hex(char * buf);

/**
 * Zwraca identyfikator urządzenia w formacie tekstowym szestnastkowym.
 * Po dwa znaki podzielone ukośnikiem + znak '\0'
 * @param buf Wskaźnik na bufor wyjściowy
 * @return Wskaźnik na koniec bufora po wypełnieniu, zawiera znak '\0'
 */
char *
device_conf_get_id_hex_path(char * buf);

void device_conf_burn_id(uint32_t new_id);
void device_conf_burn_conf(struct DeviceConf * new_conf_ptr);

extern const rom uint16_t firmware_version;

#endif //#ifndef _DEVICE_ID_H_
