#include "contiki-conf.h"
#include "TCPIPStack/Tick.h"

#include "sys/clock.h"
//#include <sys/time.h>


/*---------------------------------------------------------------------------*/
clock_time_t
clock_time(void)
{
    return ((clock_time_t)TickGet());
}
/*---------------------------------------------------------------------------*/
void
clock_delay(unsigned int d)
{
    /* Does not do anything. */
}
/*---------------------------------------------------------------------------*/

unsigned long clock_seconds(void)
{

    return TickGet() /CLOCK_SECOND;
}
