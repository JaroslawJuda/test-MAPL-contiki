#include "contiki-conf.h"
#include "TCPIPStack/TCPIP.h"
#include "device-conf.h"
#include "device-custom-ID.h"



const rom uint32_t device_conf_ID @0x1FC00 =
        (uint32_t) SERIAL_NUMBER_BYTE_0 |
        (uint32_t) SERIAL_NUMBER_BYTE_1 << 8ul |
        (uint32_t) SERIAL_NUMBER_BYTE_2 << 16ul |
        (uint32_t) SERIAL_NUMBER_RESERVED_FUTURE_USE << 24ul;



const rom struct DeviceConf device_conf@0x1FC40= { 0xffu};




// zarezerwowane miejsce na numer wersji programu
// bootloader na zakończenie programowania nowej wersji
// wypala tu właściwy numer
// muszą być same jedynki
const rom uint16_t firmware_version = 0xffffu;
//Żeby dane zaprogramowały się
//musi byc przynajmniej jedno zero na fragment 512 bajtów
static const rom uint16_t delimiter = 0u;


BYTE btohexa_low(BYTE b);
//{
//	b &= 0x0F;
//	return (b>9u) ? b+'A'-10:b+'0';
//}

BYTE btohexa_high(BYTE b);
//{
//	b >>= 4;
//	return (b>0x9u) ? b+'A'-10:b+'0';
//}

char *
device_conf_get_id_hex(char * buf) {
    uint8_t i;
    const rom uint8_t *p;
    p = (const rom uint8_t*) & device_conf_ID;
    p += 2;
    for (i = 0; i < DEVICE_ID_SERIAL_LENGTH; i += 2) {
        *buf++ = btohexa_high(*p);
        *buf++ = btohexa_low(*p);
        p--;
    }
    *buf = '\0';
    return buf;
}

char *
device_conf_get_id_hex_path(char * buf) {
    uint8_t i;
    const rom uint8_t *p;
    p = (const rom uint8_t*) & device_conf_ID;
    p += 2;
    for (i = 0; i < DEVICE_ID_SERIAL_LENGTH; i += 2) {
        *buf++ = '/';
        *buf++ = btohexa_high(*p);
        *buf++ = btohexa_low(*p);
        p--;
    }
    *buf = '\0';
    return buf;
}


#ifdef STACK_USE_HTTP2_SERVER

void HTTPPrint_deviceid(void) {
    BYTE buf[DEVICE_ID_SERIAL_LENGTH + 1];
    device_conf_get_id_hex(buf);
    TCPPutString(sktHTTP, buf);
}

void HTTPPrint_config_has_8_ports(void) {
        TCPPutROMString(sktHTTP, (ROM BYTE*) "checked=\"checked\"");
    return;
}

void HTTPPrint_config_has_4_ports(void) {

    return;
}


#endif

#ifndef TCPIP_STACK_VERSION
BYTE btohexa_low(BYTE b)
{
	b &= 0x0F;
	return (b>9u) ? b+'A'-10:b+'0';
}

BYTE btohexa_high(BYTE b)
{
	b >>= 4;
	return (b>0x9u) ? b+'A'-10:b+'0';
}

#endif
