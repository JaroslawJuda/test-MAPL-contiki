#!/bin/bash
ME=$USER
sudo tunctl -u $ME -t tap0
sudo ifconfig tap0 172.18.0.1 up
sudo ifconfig tap0 inet 172.18.0.1/16
sudo route add -net 172.18.0.0/16 dev tap0


