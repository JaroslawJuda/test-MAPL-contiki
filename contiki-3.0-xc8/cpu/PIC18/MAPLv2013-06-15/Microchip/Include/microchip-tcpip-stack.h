/* 
 * File:   microchip-tcpip-stack.h
 * Author: jarojuda
 *
 * Created on 23 wrzesień 2015, 15:30
 */

#ifndef MICROCHIP_TCPIP_STACK_H
#define	MICROCHIP_TCPIP_STACK_H

#include "contiki.h"
#include "TCPIPStack/TCPIP.h"

PROCESS_NAME(microchip_tcpip_stack_process);

#endif	/* MICROCHIP_TCPIP_STACK_H */
