#ifndef COMPILER_H
#define COMPILER_H

// Include proper device header file
#if defined(__XC8)
#include <xc.h>
#else
#error Unknown processor or compiler.  See Compiler.h
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// Base RAM and ROM pointer types for given architecture

#define PTR_BASE		unsigned short
#define ROM_PTR_BASE	unsigned long


// Definitions that apply to all except Microchip MPLAB C Compiler for PIC18 MCUs (C18)

#define memcmppgm2ram(a,b,c)	memcmp(a,b,c)
#define strcmppgm2ram(a,b)		strcmp(a,b)
#define memcpypgm2ram(a,b,c)	memcpy(a,b,c)
#define strcpypgm2ram(a,b)		strcpy(a,b)
#define strncpypgm2ram(a,b,c)	strncpy(a,b,c)
#define strstrrampgm(a,b)		strstr(a,b)
#define	strlenpgm(a)			strlen(a)
#define strchrpgm(a,b)			strchr(a,b)
#define strcatpgm2ram(a,b)		strcat(a,b)



// Definitions that apply to all 8-bit products
// (PIC10, PIC12, PIC16, PIC18)
#define	__attribute__(a)

#define FAR                         far

// HI TECH specific defines
#define ROM                 	const
#define rom

#endif
