#include "contiki.h"
#include "microchip-tcpip-stack.h"

void InitAppConfig(void);

PROCESS(microchip_tcpip_stack_process, "Microchip TCPIP Stack");

static void pollhandler(void) {
    StackTask();
    StackApplications();
    process_poll(&microchip_tcpip_stack_process);
}

PROCESS_THREAD(microchip_tcpip_stack_process, ev, data) {
    PROCESS_POLLHANDLER(pollhandler());

    PROCESS_BEGIN();
#if defined(STACK_USE_MPFS2)
    MPFSInit();
#endif
    // Initialize Stack and application related NV variables into AppConfig.
    InitAppConfig();
    // Initialize core stack layers (MAC, ARP, TCP, UDP) and
    // application modules (HTTP, SNMP, etc.)
    StackInit();

    process_poll(&microchip_tcpip_stack_process);

    PROCESS_WAIT_UNTIL(ev == PROCESS_EVENT_EXIT);

    PROCESS_END();
}

