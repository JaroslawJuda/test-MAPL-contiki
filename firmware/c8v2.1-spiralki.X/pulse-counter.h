/* 
 * File:   pulse-counter.h
 * Author: jarojuda
 *
 * Created on 2 październik 2015, 14:35
 */

#ifndef PULSE_COUNTER_H
#define	PULSE_COUNTER_H

#include "contiki.h"
#include "sampling.h"

struct PulseCounters {
    uint32_t all_cycles;
    uint32_t full_right_cycles;
    uint32_t full_wrong_cycles;
    uint32_t cycles_with_pulses;
};

#define PULSE_COUNTER_COUNTERS_NBR (SAMPLING_CHANNELS_NBR)

PROCESS_NAME(pulse_counter);

/**
 * Funkcja inicjalizuje odliczanie i zeruje liczniki
 */
void pulse_counter_reset(void) ;

#endif	/* PULSE_COUNTER_H */

