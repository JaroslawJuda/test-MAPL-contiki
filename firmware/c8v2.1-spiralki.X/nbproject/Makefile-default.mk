#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=cof
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/c8v2.1-spiralki.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/c8v2.1-spiralki.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../../contiki-3.0-xc8/core/lib/strncasecmp/strncasecmp.c ../../contiki-3.0-xc8/core/lib/aes-128.c ../../contiki-3.0-xc8/core/lib/assert.c ../../contiki-3.0-xc8/core/lib/ccm-star.c ../../contiki-3.0-xc8/core/lib/crc16.c ../../contiki-3.0-xc8/core/lib/gcr.c ../../contiki-3.0-xc8/core/lib/ifft.c ../../contiki-3.0-xc8/core/lib/list.c ../../contiki-3.0-xc8/core/lib/me.c ../../contiki-3.0-xc8/core/lib/memb.c ../../contiki-3.0-xc8/core/lib/me_tabs.c ../../contiki-3.0-xc8/core/lib/mmem.c ../../contiki-3.0-xc8/core/lib/petsciiconv.c ../../contiki-3.0-xc8/core/lib/random.c ../../contiki-3.0-xc8/core/lib/ringbuf.c ../../contiki-3.0-xc8/core/lib/settings.c ../../contiki-3.0-xc8/core/sys/arg.c ../../contiki-3.0-xc8/core/sys/autostart.c ../../contiki-3.0-xc8/core/sys/ctimer.c ../../contiki-3.0-xc8/core/sys/energest.c ../../contiki-3.0-xc8/core/sys/etimer.c ../../contiki-3.0-xc8/core/sys/process.c ../../contiki-3.0-xc8/core/sys/procinit.c ../../contiki-3.0-xc8/core/sys/rtimer.c ../../contiki-3.0-xc8/core/sys/stimer.c ../../contiki-3.0-xc8/core/sys/timer.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DNSs.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/MPFS2.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Announce.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ARCFOUR.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ARP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/AutoIP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/BerkeleyAPI.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/BigInt.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Delay.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DHCP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DHCPs.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DNS.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DynDNS.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ENC28J60.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ENCX24J600.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ETH97J60.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/FileSystem.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/FTP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Hashes.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Helpers.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/HTTP2.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ICMP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/IP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/LCDBlocking.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/NBNS.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Random.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Reboot.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/RSA.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SMTP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SNMP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SNTP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SPIEEPROM.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SPIRAM.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SSL.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/StackTsk.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/TCP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/TCPPerformanceTest.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Telnet.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/TFTPc.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Tick.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UART.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UART2TCPBridge.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UDP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UDPPerformanceTest.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/microchip-tcpip-stack.c ../../contiki-3.0-xc8/platform/c8v2.1/dev/ports-adc.c ../../contiki-3.0-xc8/platform/c8v2.1/dev/ports-leds.c ../../contiki-3.0-xc8/platform/c8v2.1/dev/ports-shifter.c ../../contiki-3.0-xc8/platform/c8v2.1/clock.c ../../contiki-3.0-xc8/platform/c8v2.1/configuration-bits.c ../../contiki-3.0-xc8/platform/c8v2.1/init-base-hardware.c ../../contiki-3.0-xc8/platform/c8v2.1/init-base-software.c ../../contiki-3.0-xc8/platform/c8v2.1/init-tcpip.c ../../contiki-3.0-xc8/platform/c8v2.1/interrupts.c ../../contiki-3.0-xc8/platform/c8v2.1/SPIFlash.c ../../contiki-3.0-xc8/platform/c8v2.1/tcpip-stack-tasks.c ../../contiki-3.0-xc8/platform/c8v2.1/device-conf.c ../../contiki-3.0-xc8/platform/c8v2.1/app-config.c ../../contiki-3.0-xc8/platform/c8v2.1/sampling.c ../../contiki-3.0-xc8/platform/c8v2.1/contiki-main.c ../../contiki-3.0-xc8/platform/c8v2.1/puls-sync.c CustomHTTPApp.c pulse_counter.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1520330545/strncasecmp.p1 ${OBJECTDIR}/_ext/1460547545/aes-128.p1 ${OBJECTDIR}/_ext/1460547545/assert.p1 ${OBJECTDIR}/_ext/1460547545/ccm-star.p1 ${OBJECTDIR}/_ext/1460547545/crc16.p1 ${OBJECTDIR}/_ext/1460547545/gcr.p1 ${OBJECTDIR}/_ext/1460547545/ifft.p1 ${OBJECTDIR}/_ext/1460547545/list.p1 ${OBJECTDIR}/_ext/1460547545/me.p1 ${OBJECTDIR}/_ext/1460547545/memb.p1 ${OBJECTDIR}/_ext/1460547545/me_tabs.p1 ${OBJECTDIR}/_ext/1460547545/mmem.p1 ${OBJECTDIR}/_ext/1460547545/petsciiconv.p1 ${OBJECTDIR}/_ext/1460547545/random.p1 ${OBJECTDIR}/_ext/1460547545/ringbuf.p1 ${OBJECTDIR}/_ext/1460547545/settings.p1 ${OBJECTDIR}/_ext/1460540305/arg.p1 ${OBJECTDIR}/_ext/1460540305/autostart.p1 ${OBJECTDIR}/_ext/1460540305/ctimer.p1 ${OBJECTDIR}/_ext/1460540305/energest.p1 ${OBJECTDIR}/_ext/1460540305/etimer.p1 ${OBJECTDIR}/_ext/1460540305/process.p1 ${OBJECTDIR}/_ext/1460540305/procinit.p1 ${OBJECTDIR}/_ext/1460540305/rtimer.p1 ${OBJECTDIR}/_ext/1460540305/stimer.p1 ${OBJECTDIR}/_ext/1460540305/timer.p1 ${OBJECTDIR}/_ext/778749567/DNSs.p1 ${OBJECTDIR}/_ext/778749567/MPFS2.p1 ${OBJECTDIR}/_ext/778749567/Announce.p1 ${OBJECTDIR}/_ext/778749567/ARCFOUR.p1 ${OBJECTDIR}/_ext/778749567/ARP.p1 ${OBJECTDIR}/_ext/778749567/AutoIP.p1 ${OBJECTDIR}/_ext/778749567/BerkeleyAPI.p1 ${OBJECTDIR}/_ext/778749567/BigInt.p1 ${OBJECTDIR}/_ext/778749567/Delay.p1 ${OBJECTDIR}/_ext/778749567/DHCP.p1 ${OBJECTDIR}/_ext/778749567/DHCPs.p1 ${OBJECTDIR}/_ext/778749567/DNS.p1 ${OBJECTDIR}/_ext/778749567/DynDNS.p1 ${OBJECTDIR}/_ext/778749567/ENC28J60.p1 ${OBJECTDIR}/_ext/778749567/ENCX24J600.p1 ${OBJECTDIR}/_ext/778749567/ETH97J60.p1 ${OBJECTDIR}/_ext/778749567/FileSystem.p1 ${OBJECTDIR}/_ext/778749567/FTP.p1 ${OBJECTDIR}/_ext/778749567/Hashes.p1 ${OBJECTDIR}/_ext/778749567/Helpers.p1 ${OBJECTDIR}/_ext/778749567/HTTP2.p1 ${OBJECTDIR}/_ext/778749567/ICMP.p1 ${OBJECTDIR}/_ext/778749567/IP.p1 ${OBJECTDIR}/_ext/778749567/LCDBlocking.p1 ${OBJECTDIR}/_ext/778749567/NBNS.p1 ${OBJECTDIR}/_ext/778749567/Random.p1 ${OBJECTDIR}/_ext/778749567/Reboot.p1 ${OBJECTDIR}/_ext/778749567/RSA.p1 ${OBJECTDIR}/_ext/778749567/SMTP.p1 ${OBJECTDIR}/_ext/778749567/SNMP.p1 ${OBJECTDIR}/_ext/778749567/SNTP.p1 ${OBJECTDIR}/_ext/778749567/SPIEEPROM.p1 ${OBJECTDIR}/_ext/778749567/SPIRAM.p1 ${OBJECTDIR}/_ext/778749567/SSL.p1 ${OBJECTDIR}/_ext/778749567/StackTsk.p1 ${OBJECTDIR}/_ext/778749567/TCP.p1 ${OBJECTDIR}/_ext/778749567/TCPPerformanceTest.p1 ${OBJECTDIR}/_ext/778749567/Telnet.p1 ${OBJECTDIR}/_ext/778749567/TFTPc.p1 ${OBJECTDIR}/_ext/778749567/Tick.p1 ${OBJECTDIR}/_ext/778749567/UART.p1 ${OBJECTDIR}/_ext/778749567/UART2TCPBridge.p1 ${OBJECTDIR}/_ext/778749567/UDP.p1 ${OBJECTDIR}/_ext/778749567/UDPPerformanceTest.p1 ${OBJECTDIR}/_ext/518696466/microchip-tcpip-stack.p1 ${OBJECTDIR}/_ext/109911484/ports-adc.p1 ${OBJECTDIR}/_ext/109911484/ports-leds.p1 ${OBJECTDIR}/_ext/109911484/ports-shifter.p1 ${OBJECTDIR}/_ext/830074946/clock.p1 ${OBJECTDIR}/_ext/830074946/configuration-bits.p1 ${OBJECTDIR}/_ext/830074946/init-base-hardware.p1 ${OBJECTDIR}/_ext/830074946/init-base-software.p1 ${OBJECTDIR}/_ext/830074946/init-tcpip.p1 ${OBJECTDIR}/_ext/830074946/interrupts.p1 ${OBJECTDIR}/_ext/830074946/SPIFlash.p1 ${OBJECTDIR}/_ext/830074946/tcpip-stack-tasks.p1 ${OBJECTDIR}/_ext/830074946/device-conf.p1 ${OBJECTDIR}/_ext/830074946/app-config.p1 ${OBJECTDIR}/_ext/830074946/sampling.p1 ${OBJECTDIR}/_ext/830074946/contiki-main.p1 ${OBJECTDIR}/_ext/830074946/puls-sync.p1 ${OBJECTDIR}/CustomHTTPApp.p1 ${OBJECTDIR}/pulse_counter.p1
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1520330545/strncasecmp.p1.d ${OBJECTDIR}/_ext/1460547545/aes-128.p1.d ${OBJECTDIR}/_ext/1460547545/assert.p1.d ${OBJECTDIR}/_ext/1460547545/ccm-star.p1.d ${OBJECTDIR}/_ext/1460547545/crc16.p1.d ${OBJECTDIR}/_ext/1460547545/gcr.p1.d ${OBJECTDIR}/_ext/1460547545/ifft.p1.d ${OBJECTDIR}/_ext/1460547545/list.p1.d ${OBJECTDIR}/_ext/1460547545/me.p1.d ${OBJECTDIR}/_ext/1460547545/memb.p1.d ${OBJECTDIR}/_ext/1460547545/me_tabs.p1.d ${OBJECTDIR}/_ext/1460547545/mmem.p1.d ${OBJECTDIR}/_ext/1460547545/petsciiconv.p1.d ${OBJECTDIR}/_ext/1460547545/random.p1.d ${OBJECTDIR}/_ext/1460547545/ringbuf.p1.d ${OBJECTDIR}/_ext/1460547545/settings.p1.d ${OBJECTDIR}/_ext/1460540305/arg.p1.d ${OBJECTDIR}/_ext/1460540305/autostart.p1.d ${OBJECTDIR}/_ext/1460540305/ctimer.p1.d ${OBJECTDIR}/_ext/1460540305/energest.p1.d ${OBJECTDIR}/_ext/1460540305/etimer.p1.d ${OBJECTDIR}/_ext/1460540305/process.p1.d ${OBJECTDIR}/_ext/1460540305/procinit.p1.d ${OBJECTDIR}/_ext/1460540305/rtimer.p1.d ${OBJECTDIR}/_ext/1460540305/stimer.p1.d ${OBJECTDIR}/_ext/1460540305/timer.p1.d ${OBJECTDIR}/_ext/778749567/DNSs.p1.d ${OBJECTDIR}/_ext/778749567/MPFS2.p1.d ${OBJECTDIR}/_ext/778749567/Announce.p1.d ${OBJECTDIR}/_ext/778749567/ARCFOUR.p1.d ${OBJECTDIR}/_ext/778749567/ARP.p1.d ${OBJECTDIR}/_ext/778749567/AutoIP.p1.d ${OBJECTDIR}/_ext/778749567/BerkeleyAPI.p1.d ${OBJECTDIR}/_ext/778749567/BigInt.p1.d ${OBJECTDIR}/_ext/778749567/Delay.p1.d ${OBJECTDIR}/_ext/778749567/DHCP.p1.d ${OBJECTDIR}/_ext/778749567/DHCPs.p1.d ${OBJECTDIR}/_ext/778749567/DNS.p1.d ${OBJECTDIR}/_ext/778749567/DynDNS.p1.d ${OBJECTDIR}/_ext/778749567/ENC28J60.p1.d ${OBJECTDIR}/_ext/778749567/ENCX24J600.p1.d ${OBJECTDIR}/_ext/778749567/ETH97J60.p1.d ${OBJECTDIR}/_ext/778749567/FileSystem.p1.d ${OBJECTDIR}/_ext/778749567/FTP.p1.d ${OBJECTDIR}/_ext/778749567/Hashes.p1.d ${OBJECTDIR}/_ext/778749567/Helpers.p1.d ${OBJECTDIR}/_ext/778749567/HTTP2.p1.d ${OBJECTDIR}/_ext/778749567/ICMP.p1.d ${OBJECTDIR}/_ext/778749567/IP.p1.d ${OBJECTDIR}/_ext/778749567/LCDBlocking.p1.d ${OBJECTDIR}/_ext/778749567/NBNS.p1.d ${OBJECTDIR}/_ext/778749567/Random.p1.d ${OBJECTDIR}/_ext/778749567/Reboot.p1.d ${OBJECTDIR}/_ext/778749567/RSA.p1.d ${OBJECTDIR}/_ext/778749567/SMTP.p1.d ${OBJECTDIR}/_ext/778749567/SNMP.p1.d ${OBJECTDIR}/_ext/778749567/SNTP.p1.d ${OBJECTDIR}/_ext/778749567/SPIEEPROM.p1.d ${OBJECTDIR}/_ext/778749567/SPIRAM.p1.d ${OBJECTDIR}/_ext/778749567/SSL.p1.d ${OBJECTDIR}/_ext/778749567/StackTsk.p1.d ${OBJECTDIR}/_ext/778749567/TCP.p1.d ${OBJECTDIR}/_ext/778749567/TCPPerformanceTest.p1.d ${OBJECTDIR}/_ext/778749567/Telnet.p1.d ${OBJECTDIR}/_ext/778749567/TFTPc.p1.d ${OBJECTDIR}/_ext/778749567/Tick.p1.d ${OBJECTDIR}/_ext/778749567/UART.p1.d ${OBJECTDIR}/_ext/778749567/UART2TCPBridge.p1.d ${OBJECTDIR}/_ext/778749567/UDP.p1.d ${OBJECTDIR}/_ext/778749567/UDPPerformanceTest.p1.d ${OBJECTDIR}/_ext/518696466/microchip-tcpip-stack.p1.d ${OBJECTDIR}/_ext/109911484/ports-adc.p1.d ${OBJECTDIR}/_ext/109911484/ports-leds.p1.d ${OBJECTDIR}/_ext/109911484/ports-shifter.p1.d ${OBJECTDIR}/_ext/830074946/clock.p1.d ${OBJECTDIR}/_ext/830074946/configuration-bits.p1.d ${OBJECTDIR}/_ext/830074946/init-base-hardware.p1.d ${OBJECTDIR}/_ext/830074946/init-base-software.p1.d ${OBJECTDIR}/_ext/830074946/init-tcpip.p1.d ${OBJECTDIR}/_ext/830074946/interrupts.p1.d ${OBJECTDIR}/_ext/830074946/SPIFlash.p1.d ${OBJECTDIR}/_ext/830074946/tcpip-stack-tasks.p1.d ${OBJECTDIR}/_ext/830074946/device-conf.p1.d ${OBJECTDIR}/_ext/830074946/app-config.p1.d ${OBJECTDIR}/_ext/830074946/sampling.p1.d ${OBJECTDIR}/_ext/830074946/contiki-main.p1.d ${OBJECTDIR}/_ext/830074946/puls-sync.p1.d ${OBJECTDIR}/CustomHTTPApp.p1.d ${OBJECTDIR}/pulse_counter.p1.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1520330545/strncasecmp.p1 ${OBJECTDIR}/_ext/1460547545/aes-128.p1 ${OBJECTDIR}/_ext/1460547545/assert.p1 ${OBJECTDIR}/_ext/1460547545/ccm-star.p1 ${OBJECTDIR}/_ext/1460547545/crc16.p1 ${OBJECTDIR}/_ext/1460547545/gcr.p1 ${OBJECTDIR}/_ext/1460547545/ifft.p1 ${OBJECTDIR}/_ext/1460547545/list.p1 ${OBJECTDIR}/_ext/1460547545/me.p1 ${OBJECTDIR}/_ext/1460547545/memb.p1 ${OBJECTDIR}/_ext/1460547545/me_tabs.p1 ${OBJECTDIR}/_ext/1460547545/mmem.p1 ${OBJECTDIR}/_ext/1460547545/petsciiconv.p1 ${OBJECTDIR}/_ext/1460547545/random.p1 ${OBJECTDIR}/_ext/1460547545/ringbuf.p1 ${OBJECTDIR}/_ext/1460547545/settings.p1 ${OBJECTDIR}/_ext/1460540305/arg.p1 ${OBJECTDIR}/_ext/1460540305/autostart.p1 ${OBJECTDIR}/_ext/1460540305/ctimer.p1 ${OBJECTDIR}/_ext/1460540305/energest.p1 ${OBJECTDIR}/_ext/1460540305/etimer.p1 ${OBJECTDIR}/_ext/1460540305/process.p1 ${OBJECTDIR}/_ext/1460540305/procinit.p1 ${OBJECTDIR}/_ext/1460540305/rtimer.p1 ${OBJECTDIR}/_ext/1460540305/stimer.p1 ${OBJECTDIR}/_ext/1460540305/timer.p1 ${OBJECTDIR}/_ext/778749567/DNSs.p1 ${OBJECTDIR}/_ext/778749567/MPFS2.p1 ${OBJECTDIR}/_ext/778749567/Announce.p1 ${OBJECTDIR}/_ext/778749567/ARCFOUR.p1 ${OBJECTDIR}/_ext/778749567/ARP.p1 ${OBJECTDIR}/_ext/778749567/AutoIP.p1 ${OBJECTDIR}/_ext/778749567/BerkeleyAPI.p1 ${OBJECTDIR}/_ext/778749567/BigInt.p1 ${OBJECTDIR}/_ext/778749567/Delay.p1 ${OBJECTDIR}/_ext/778749567/DHCP.p1 ${OBJECTDIR}/_ext/778749567/DHCPs.p1 ${OBJECTDIR}/_ext/778749567/DNS.p1 ${OBJECTDIR}/_ext/778749567/DynDNS.p1 ${OBJECTDIR}/_ext/778749567/ENC28J60.p1 ${OBJECTDIR}/_ext/778749567/ENCX24J600.p1 ${OBJECTDIR}/_ext/778749567/ETH97J60.p1 ${OBJECTDIR}/_ext/778749567/FileSystem.p1 ${OBJECTDIR}/_ext/778749567/FTP.p1 ${OBJECTDIR}/_ext/778749567/Hashes.p1 ${OBJECTDIR}/_ext/778749567/Helpers.p1 ${OBJECTDIR}/_ext/778749567/HTTP2.p1 ${OBJECTDIR}/_ext/778749567/ICMP.p1 ${OBJECTDIR}/_ext/778749567/IP.p1 ${OBJECTDIR}/_ext/778749567/LCDBlocking.p1 ${OBJECTDIR}/_ext/778749567/NBNS.p1 ${OBJECTDIR}/_ext/778749567/Random.p1 ${OBJECTDIR}/_ext/778749567/Reboot.p1 ${OBJECTDIR}/_ext/778749567/RSA.p1 ${OBJECTDIR}/_ext/778749567/SMTP.p1 ${OBJECTDIR}/_ext/778749567/SNMP.p1 ${OBJECTDIR}/_ext/778749567/SNTP.p1 ${OBJECTDIR}/_ext/778749567/SPIEEPROM.p1 ${OBJECTDIR}/_ext/778749567/SPIRAM.p1 ${OBJECTDIR}/_ext/778749567/SSL.p1 ${OBJECTDIR}/_ext/778749567/StackTsk.p1 ${OBJECTDIR}/_ext/778749567/TCP.p1 ${OBJECTDIR}/_ext/778749567/TCPPerformanceTest.p1 ${OBJECTDIR}/_ext/778749567/Telnet.p1 ${OBJECTDIR}/_ext/778749567/TFTPc.p1 ${OBJECTDIR}/_ext/778749567/Tick.p1 ${OBJECTDIR}/_ext/778749567/UART.p1 ${OBJECTDIR}/_ext/778749567/UART2TCPBridge.p1 ${OBJECTDIR}/_ext/778749567/UDP.p1 ${OBJECTDIR}/_ext/778749567/UDPPerformanceTest.p1 ${OBJECTDIR}/_ext/518696466/microchip-tcpip-stack.p1 ${OBJECTDIR}/_ext/109911484/ports-adc.p1 ${OBJECTDIR}/_ext/109911484/ports-leds.p1 ${OBJECTDIR}/_ext/109911484/ports-shifter.p1 ${OBJECTDIR}/_ext/830074946/clock.p1 ${OBJECTDIR}/_ext/830074946/configuration-bits.p1 ${OBJECTDIR}/_ext/830074946/init-base-hardware.p1 ${OBJECTDIR}/_ext/830074946/init-base-software.p1 ${OBJECTDIR}/_ext/830074946/init-tcpip.p1 ${OBJECTDIR}/_ext/830074946/interrupts.p1 ${OBJECTDIR}/_ext/830074946/SPIFlash.p1 ${OBJECTDIR}/_ext/830074946/tcpip-stack-tasks.p1 ${OBJECTDIR}/_ext/830074946/device-conf.p1 ${OBJECTDIR}/_ext/830074946/app-config.p1 ${OBJECTDIR}/_ext/830074946/sampling.p1 ${OBJECTDIR}/_ext/830074946/contiki-main.p1 ${OBJECTDIR}/_ext/830074946/puls-sync.p1 ${OBJECTDIR}/CustomHTTPApp.p1 ${OBJECTDIR}/pulse_counter.p1

# Source Files
SOURCEFILES=../../contiki-3.0-xc8/core/lib/strncasecmp/strncasecmp.c ../../contiki-3.0-xc8/core/lib/aes-128.c ../../contiki-3.0-xc8/core/lib/assert.c ../../contiki-3.0-xc8/core/lib/ccm-star.c ../../contiki-3.0-xc8/core/lib/crc16.c ../../contiki-3.0-xc8/core/lib/gcr.c ../../contiki-3.0-xc8/core/lib/ifft.c ../../contiki-3.0-xc8/core/lib/list.c ../../contiki-3.0-xc8/core/lib/me.c ../../contiki-3.0-xc8/core/lib/memb.c ../../contiki-3.0-xc8/core/lib/me_tabs.c ../../contiki-3.0-xc8/core/lib/mmem.c ../../contiki-3.0-xc8/core/lib/petsciiconv.c ../../contiki-3.0-xc8/core/lib/random.c ../../contiki-3.0-xc8/core/lib/ringbuf.c ../../contiki-3.0-xc8/core/lib/settings.c ../../contiki-3.0-xc8/core/sys/arg.c ../../contiki-3.0-xc8/core/sys/autostart.c ../../contiki-3.0-xc8/core/sys/ctimer.c ../../contiki-3.0-xc8/core/sys/energest.c ../../contiki-3.0-xc8/core/sys/etimer.c ../../contiki-3.0-xc8/core/sys/process.c ../../contiki-3.0-xc8/core/sys/procinit.c ../../contiki-3.0-xc8/core/sys/rtimer.c ../../contiki-3.0-xc8/core/sys/stimer.c ../../contiki-3.0-xc8/core/sys/timer.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DNSs.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/MPFS2.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Announce.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ARCFOUR.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ARP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/AutoIP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/BerkeleyAPI.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/BigInt.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Delay.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DHCP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DHCPs.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DNS.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DynDNS.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ENC28J60.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ENCX24J600.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ETH97J60.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/FileSystem.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/FTP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Hashes.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Helpers.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/HTTP2.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ICMP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/IP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/LCDBlocking.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/NBNS.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Random.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Reboot.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/RSA.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SMTP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SNMP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SNTP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SPIEEPROM.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SPIRAM.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SSL.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/StackTsk.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/TCP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/TCPPerformanceTest.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Telnet.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/TFTPc.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Tick.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UART.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UART2TCPBridge.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UDP.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UDPPerformanceTest.c ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/microchip-tcpip-stack.c ../../contiki-3.0-xc8/platform/c8v2.1/dev/ports-adc.c ../../contiki-3.0-xc8/platform/c8v2.1/dev/ports-leds.c ../../contiki-3.0-xc8/platform/c8v2.1/dev/ports-shifter.c ../../contiki-3.0-xc8/platform/c8v2.1/clock.c ../../contiki-3.0-xc8/platform/c8v2.1/configuration-bits.c ../../contiki-3.0-xc8/platform/c8v2.1/init-base-hardware.c ../../contiki-3.0-xc8/platform/c8v2.1/init-base-software.c ../../contiki-3.0-xc8/platform/c8v2.1/init-tcpip.c ../../contiki-3.0-xc8/platform/c8v2.1/interrupts.c ../../contiki-3.0-xc8/platform/c8v2.1/SPIFlash.c ../../contiki-3.0-xc8/platform/c8v2.1/tcpip-stack-tasks.c ../../contiki-3.0-xc8/platform/c8v2.1/device-conf.c ../../contiki-3.0-xc8/platform/c8v2.1/app-config.c ../../contiki-3.0-xc8/platform/c8v2.1/sampling.c ../../contiki-3.0-xc8/platform/c8v2.1/contiki-main.c ../../contiki-3.0-xc8/platform/c8v2.1/puls-sync.c CustomHTTPApp.c pulse_counter.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/c8v2.1-spiralki.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F67J60
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1520330545/strncasecmp.p1: ../../contiki-3.0-xc8/core/lib/strncasecmp/strncasecmp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1520330545" 
	@${RM} ${OBJECTDIR}/_ext/1520330545/strncasecmp.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1520330545/strncasecmp.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1520330545/strncasecmp.p1  ../../contiki-3.0-xc8/core/lib/strncasecmp/strncasecmp.c 
	@-${MV} ${OBJECTDIR}/_ext/1520330545/strncasecmp.d ${OBJECTDIR}/_ext/1520330545/strncasecmp.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1520330545/strncasecmp.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/aes-128.p1: ../../contiki-3.0-xc8/core/lib/aes-128.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/aes-128.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/aes-128.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/aes-128.p1  ../../contiki-3.0-xc8/core/lib/aes-128.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/aes-128.d ${OBJECTDIR}/_ext/1460547545/aes-128.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/aes-128.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/assert.p1: ../../contiki-3.0-xc8/core/lib/assert.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/assert.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/assert.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/assert.p1  ../../contiki-3.0-xc8/core/lib/assert.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/assert.d ${OBJECTDIR}/_ext/1460547545/assert.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/assert.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/ccm-star.p1: ../../contiki-3.0-xc8/core/lib/ccm-star.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/ccm-star.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/ccm-star.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/ccm-star.p1  ../../contiki-3.0-xc8/core/lib/ccm-star.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/ccm-star.d ${OBJECTDIR}/_ext/1460547545/ccm-star.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/ccm-star.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/crc16.p1: ../../contiki-3.0-xc8/core/lib/crc16.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/crc16.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/crc16.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/crc16.p1  ../../contiki-3.0-xc8/core/lib/crc16.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/crc16.d ${OBJECTDIR}/_ext/1460547545/crc16.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/crc16.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/gcr.p1: ../../contiki-3.0-xc8/core/lib/gcr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/gcr.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/gcr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/gcr.p1  ../../contiki-3.0-xc8/core/lib/gcr.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/gcr.d ${OBJECTDIR}/_ext/1460547545/gcr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/gcr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/ifft.p1: ../../contiki-3.0-xc8/core/lib/ifft.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/ifft.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/ifft.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/ifft.p1  ../../contiki-3.0-xc8/core/lib/ifft.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/ifft.d ${OBJECTDIR}/_ext/1460547545/ifft.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/ifft.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/list.p1: ../../contiki-3.0-xc8/core/lib/list.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/list.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/list.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/list.p1  ../../contiki-3.0-xc8/core/lib/list.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/list.d ${OBJECTDIR}/_ext/1460547545/list.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/list.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/me.p1: ../../contiki-3.0-xc8/core/lib/me.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/me.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/me.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/me.p1  ../../contiki-3.0-xc8/core/lib/me.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/me.d ${OBJECTDIR}/_ext/1460547545/me.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/me.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/memb.p1: ../../contiki-3.0-xc8/core/lib/memb.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/memb.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/memb.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/memb.p1  ../../contiki-3.0-xc8/core/lib/memb.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/memb.d ${OBJECTDIR}/_ext/1460547545/memb.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/memb.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/me_tabs.p1: ../../contiki-3.0-xc8/core/lib/me_tabs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/me_tabs.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/me_tabs.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/me_tabs.p1  ../../contiki-3.0-xc8/core/lib/me_tabs.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/me_tabs.d ${OBJECTDIR}/_ext/1460547545/me_tabs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/me_tabs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/mmem.p1: ../../contiki-3.0-xc8/core/lib/mmem.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/mmem.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/mmem.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/mmem.p1  ../../contiki-3.0-xc8/core/lib/mmem.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/mmem.d ${OBJECTDIR}/_ext/1460547545/mmem.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/mmem.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/petsciiconv.p1: ../../contiki-3.0-xc8/core/lib/petsciiconv.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/petsciiconv.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/petsciiconv.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/petsciiconv.p1  ../../contiki-3.0-xc8/core/lib/petsciiconv.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/petsciiconv.d ${OBJECTDIR}/_ext/1460547545/petsciiconv.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/petsciiconv.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/random.p1: ../../contiki-3.0-xc8/core/lib/random.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/random.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/random.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/random.p1  ../../contiki-3.0-xc8/core/lib/random.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/random.d ${OBJECTDIR}/_ext/1460547545/random.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/random.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/ringbuf.p1: ../../contiki-3.0-xc8/core/lib/ringbuf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/ringbuf.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/ringbuf.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/ringbuf.p1  ../../contiki-3.0-xc8/core/lib/ringbuf.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/ringbuf.d ${OBJECTDIR}/_ext/1460547545/ringbuf.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/ringbuf.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/settings.p1: ../../contiki-3.0-xc8/core/lib/settings.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/settings.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/settings.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/settings.p1  ../../contiki-3.0-xc8/core/lib/settings.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/settings.d ${OBJECTDIR}/_ext/1460547545/settings.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/settings.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/arg.p1: ../../contiki-3.0-xc8/core/sys/arg.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/arg.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/arg.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/arg.p1  ../../contiki-3.0-xc8/core/sys/arg.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/arg.d ${OBJECTDIR}/_ext/1460540305/arg.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/arg.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/autostart.p1: ../../contiki-3.0-xc8/core/sys/autostart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/autostart.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/autostart.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/autostart.p1  ../../contiki-3.0-xc8/core/sys/autostart.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/autostart.d ${OBJECTDIR}/_ext/1460540305/autostart.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/autostart.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/ctimer.p1: ../../contiki-3.0-xc8/core/sys/ctimer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/ctimer.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/ctimer.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/ctimer.p1  ../../contiki-3.0-xc8/core/sys/ctimer.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/ctimer.d ${OBJECTDIR}/_ext/1460540305/ctimer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/ctimer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/energest.p1: ../../contiki-3.0-xc8/core/sys/energest.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/energest.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/energest.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/energest.p1  ../../contiki-3.0-xc8/core/sys/energest.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/energest.d ${OBJECTDIR}/_ext/1460540305/energest.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/energest.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/etimer.p1: ../../contiki-3.0-xc8/core/sys/etimer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/etimer.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/etimer.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/etimer.p1  ../../contiki-3.0-xc8/core/sys/etimer.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/etimer.d ${OBJECTDIR}/_ext/1460540305/etimer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/etimer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/process.p1: ../../contiki-3.0-xc8/core/sys/process.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/process.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/process.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/process.p1  ../../contiki-3.0-xc8/core/sys/process.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/process.d ${OBJECTDIR}/_ext/1460540305/process.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/process.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/procinit.p1: ../../contiki-3.0-xc8/core/sys/procinit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/procinit.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/procinit.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/procinit.p1  ../../contiki-3.0-xc8/core/sys/procinit.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/procinit.d ${OBJECTDIR}/_ext/1460540305/procinit.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/procinit.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/rtimer.p1: ../../contiki-3.0-xc8/core/sys/rtimer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/rtimer.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/rtimer.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/rtimer.p1  ../../contiki-3.0-xc8/core/sys/rtimer.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/rtimer.d ${OBJECTDIR}/_ext/1460540305/rtimer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/rtimer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/stimer.p1: ../../contiki-3.0-xc8/core/sys/stimer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/stimer.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/stimer.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/stimer.p1  ../../contiki-3.0-xc8/core/sys/stimer.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/stimer.d ${OBJECTDIR}/_ext/1460540305/stimer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/stimer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/timer.p1: ../../contiki-3.0-xc8/core/sys/timer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/timer.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/timer.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/timer.p1  ../../contiki-3.0-xc8/core/sys/timer.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/timer.d ${OBJECTDIR}/_ext/1460540305/timer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/timer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/DNSs.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DNSs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/DNSs.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/DNSs.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/DNSs.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DNSs.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/DNSs.d ${OBJECTDIR}/_ext/778749567/DNSs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/DNSs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/MPFS2.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/MPFS2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/MPFS2.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/MPFS2.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/MPFS2.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/MPFS2.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/MPFS2.d ${OBJECTDIR}/_ext/778749567/MPFS2.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/MPFS2.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/Announce.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Announce.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/Announce.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/Announce.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/Announce.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Announce.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/Announce.d ${OBJECTDIR}/_ext/778749567/Announce.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/Announce.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/ARCFOUR.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ARCFOUR.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/ARCFOUR.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/ARCFOUR.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/ARCFOUR.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ARCFOUR.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/ARCFOUR.d ${OBJECTDIR}/_ext/778749567/ARCFOUR.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/ARCFOUR.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/ARP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ARP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/ARP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/ARP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/ARP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ARP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/ARP.d ${OBJECTDIR}/_ext/778749567/ARP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/ARP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/AutoIP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/AutoIP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/AutoIP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/AutoIP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/AutoIP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/AutoIP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/AutoIP.d ${OBJECTDIR}/_ext/778749567/AutoIP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/AutoIP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/BerkeleyAPI.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/BerkeleyAPI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/BerkeleyAPI.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/BerkeleyAPI.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/BerkeleyAPI.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/BerkeleyAPI.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/BerkeleyAPI.d ${OBJECTDIR}/_ext/778749567/BerkeleyAPI.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/BerkeleyAPI.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/BigInt.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/BigInt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/BigInt.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/BigInt.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/BigInt.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/BigInt.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/BigInt.d ${OBJECTDIR}/_ext/778749567/BigInt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/BigInt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/Delay.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/Delay.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/Delay.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/Delay.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Delay.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/Delay.d ${OBJECTDIR}/_ext/778749567/Delay.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/Delay.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/DHCP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DHCP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/DHCP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/DHCP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/DHCP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DHCP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/DHCP.d ${OBJECTDIR}/_ext/778749567/DHCP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/DHCP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/DHCPs.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DHCPs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/DHCPs.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/DHCPs.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/DHCPs.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DHCPs.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/DHCPs.d ${OBJECTDIR}/_ext/778749567/DHCPs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/DHCPs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/DNS.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DNS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/DNS.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/DNS.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/DNS.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DNS.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/DNS.d ${OBJECTDIR}/_ext/778749567/DNS.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/DNS.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/DynDNS.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DynDNS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/DynDNS.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/DynDNS.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/DynDNS.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DynDNS.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/DynDNS.d ${OBJECTDIR}/_ext/778749567/DynDNS.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/DynDNS.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/ENC28J60.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ENC28J60.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/ENC28J60.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/ENC28J60.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/ENC28J60.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ENC28J60.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/ENC28J60.d ${OBJECTDIR}/_ext/778749567/ENC28J60.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/ENC28J60.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/ENCX24J600.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ENCX24J600.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/ENCX24J600.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/ENCX24J600.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/ENCX24J600.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ENCX24J600.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/ENCX24J600.d ${OBJECTDIR}/_ext/778749567/ENCX24J600.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/ENCX24J600.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/ETH97J60.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ETH97J60.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/ETH97J60.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/ETH97J60.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/ETH97J60.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ETH97J60.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/ETH97J60.d ${OBJECTDIR}/_ext/778749567/ETH97J60.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/ETH97J60.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/FileSystem.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/FileSystem.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/FileSystem.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/FileSystem.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/FileSystem.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/FileSystem.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/FileSystem.d ${OBJECTDIR}/_ext/778749567/FileSystem.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/FileSystem.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/FTP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/FTP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/FTP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/FTP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/FTP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/FTP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/FTP.d ${OBJECTDIR}/_ext/778749567/FTP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/FTP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/Hashes.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Hashes.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/Hashes.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/Hashes.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/Hashes.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Hashes.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/Hashes.d ${OBJECTDIR}/_ext/778749567/Hashes.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/Hashes.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/Helpers.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Helpers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/Helpers.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/Helpers.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/Helpers.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Helpers.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/Helpers.d ${OBJECTDIR}/_ext/778749567/Helpers.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/Helpers.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/HTTP2.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/HTTP2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/HTTP2.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/HTTP2.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/HTTP2.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/HTTP2.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/HTTP2.d ${OBJECTDIR}/_ext/778749567/HTTP2.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/HTTP2.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/ICMP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ICMP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/ICMP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/ICMP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/ICMP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ICMP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/ICMP.d ${OBJECTDIR}/_ext/778749567/ICMP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/ICMP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/IP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/IP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/IP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/IP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/IP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/IP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/IP.d ${OBJECTDIR}/_ext/778749567/IP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/IP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/LCDBlocking.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/LCDBlocking.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/LCDBlocking.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/LCDBlocking.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/LCDBlocking.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/LCDBlocking.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/LCDBlocking.d ${OBJECTDIR}/_ext/778749567/LCDBlocking.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/LCDBlocking.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/NBNS.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/NBNS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/NBNS.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/NBNS.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/NBNS.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/NBNS.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/NBNS.d ${OBJECTDIR}/_ext/778749567/NBNS.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/NBNS.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/Random.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Random.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/Random.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/Random.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/Random.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Random.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/Random.d ${OBJECTDIR}/_ext/778749567/Random.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/Random.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/Reboot.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Reboot.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/Reboot.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/Reboot.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/Reboot.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Reboot.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/Reboot.d ${OBJECTDIR}/_ext/778749567/Reboot.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/Reboot.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/RSA.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/RSA.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/RSA.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/RSA.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/RSA.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/RSA.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/RSA.d ${OBJECTDIR}/_ext/778749567/RSA.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/RSA.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/SMTP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SMTP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/SMTP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/SMTP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/SMTP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SMTP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/SMTP.d ${OBJECTDIR}/_ext/778749567/SMTP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/SMTP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/SNMP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SNMP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/SNMP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/SNMP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/SNMP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SNMP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/SNMP.d ${OBJECTDIR}/_ext/778749567/SNMP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/SNMP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/SNTP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SNTP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/SNTP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/SNTP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/SNTP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SNTP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/SNTP.d ${OBJECTDIR}/_ext/778749567/SNTP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/SNTP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/SPIEEPROM.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SPIEEPROM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/SPIEEPROM.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/SPIEEPROM.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/SPIEEPROM.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SPIEEPROM.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/SPIEEPROM.d ${OBJECTDIR}/_ext/778749567/SPIEEPROM.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/SPIEEPROM.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/SPIRAM.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SPIRAM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/SPIRAM.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/SPIRAM.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/SPIRAM.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SPIRAM.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/SPIRAM.d ${OBJECTDIR}/_ext/778749567/SPIRAM.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/SPIRAM.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/SSL.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SSL.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/SSL.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/SSL.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/SSL.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SSL.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/SSL.d ${OBJECTDIR}/_ext/778749567/SSL.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/SSL.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/StackTsk.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/StackTsk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/StackTsk.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/StackTsk.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/StackTsk.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/StackTsk.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/StackTsk.d ${OBJECTDIR}/_ext/778749567/StackTsk.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/StackTsk.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/TCP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/TCP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/TCP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/TCP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/TCP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/TCP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/TCP.d ${OBJECTDIR}/_ext/778749567/TCP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/TCP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/TCPPerformanceTest.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/TCPPerformanceTest.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/TCPPerformanceTest.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/TCPPerformanceTest.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/TCPPerformanceTest.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/TCPPerformanceTest.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/TCPPerformanceTest.d ${OBJECTDIR}/_ext/778749567/TCPPerformanceTest.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/TCPPerformanceTest.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/Telnet.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Telnet.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/Telnet.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/Telnet.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/Telnet.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Telnet.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/Telnet.d ${OBJECTDIR}/_ext/778749567/Telnet.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/Telnet.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/TFTPc.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/TFTPc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/TFTPc.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/TFTPc.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/TFTPc.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/TFTPc.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/TFTPc.d ${OBJECTDIR}/_ext/778749567/TFTPc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/TFTPc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/Tick.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Tick.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/Tick.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/Tick.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/Tick.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Tick.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/Tick.d ${OBJECTDIR}/_ext/778749567/Tick.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/Tick.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/UART.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UART.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/UART.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/UART.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/UART.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UART.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/UART.d ${OBJECTDIR}/_ext/778749567/UART.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/UART.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/UART2TCPBridge.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UART2TCPBridge.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/UART2TCPBridge.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/UART2TCPBridge.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/UART2TCPBridge.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UART2TCPBridge.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/UART2TCPBridge.d ${OBJECTDIR}/_ext/778749567/UART2TCPBridge.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/UART2TCPBridge.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/UDP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UDP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/UDP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/UDP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/UDP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UDP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/UDP.d ${OBJECTDIR}/_ext/778749567/UDP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/UDP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/UDPPerformanceTest.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UDPPerformanceTest.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/UDPPerformanceTest.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/UDPPerformanceTest.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/UDPPerformanceTest.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UDPPerformanceTest.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/UDPPerformanceTest.d ${OBJECTDIR}/_ext/778749567/UDPPerformanceTest.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/UDPPerformanceTest.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/518696466/microchip-tcpip-stack.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/microchip-tcpip-stack.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/518696466" 
	@${RM} ${OBJECTDIR}/_ext/518696466/microchip-tcpip-stack.p1.d 
	@${RM} ${OBJECTDIR}/_ext/518696466/microchip-tcpip-stack.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/518696466/microchip-tcpip-stack.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/microchip-tcpip-stack.c 
	@-${MV} ${OBJECTDIR}/_ext/518696466/microchip-tcpip-stack.d ${OBJECTDIR}/_ext/518696466/microchip-tcpip-stack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/518696466/microchip-tcpip-stack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/109911484/ports-adc.p1: ../../contiki-3.0-xc8/platform/c8v2.1/dev/ports-adc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/109911484" 
	@${RM} ${OBJECTDIR}/_ext/109911484/ports-adc.p1.d 
	@${RM} ${OBJECTDIR}/_ext/109911484/ports-adc.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/109911484/ports-adc.p1  ../../contiki-3.0-xc8/platform/c8v2.1/dev/ports-adc.c 
	@-${MV} ${OBJECTDIR}/_ext/109911484/ports-adc.d ${OBJECTDIR}/_ext/109911484/ports-adc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/109911484/ports-adc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/109911484/ports-leds.p1: ../../contiki-3.0-xc8/platform/c8v2.1/dev/ports-leds.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/109911484" 
	@${RM} ${OBJECTDIR}/_ext/109911484/ports-leds.p1.d 
	@${RM} ${OBJECTDIR}/_ext/109911484/ports-leds.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/109911484/ports-leds.p1  ../../contiki-3.0-xc8/platform/c8v2.1/dev/ports-leds.c 
	@-${MV} ${OBJECTDIR}/_ext/109911484/ports-leds.d ${OBJECTDIR}/_ext/109911484/ports-leds.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/109911484/ports-leds.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/109911484/ports-shifter.p1: ../../contiki-3.0-xc8/platform/c8v2.1/dev/ports-shifter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/109911484" 
	@${RM} ${OBJECTDIR}/_ext/109911484/ports-shifter.p1.d 
	@${RM} ${OBJECTDIR}/_ext/109911484/ports-shifter.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/109911484/ports-shifter.p1  ../../contiki-3.0-xc8/platform/c8v2.1/dev/ports-shifter.c 
	@-${MV} ${OBJECTDIR}/_ext/109911484/ports-shifter.d ${OBJECTDIR}/_ext/109911484/ports-shifter.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/109911484/ports-shifter.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/clock.p1: ../../contiki-3.0-xc8/platform/c8v2.1/clock.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/clock.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/clock.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/clock.p1  ../../contiki-3.0-xc8/platform/c8v2.1/clock.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/clock.d ${OBJECTDIR}/_ext/830074946/clock.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/clock.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/configuration-bits.p1: ../../contiki-3.0-xc8/platform/c8v2.1/configuration-bits.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/configuration-bits.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/configuration-bits.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/configuration-bits.p1  ../../contiki-3.0-xc8/platform/c8v2.1/configuration-bits.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/configuration-bits.d ${OBJECTDIR}/_ext/830074946/configuration-bits.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/configuration-bits.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/init-base-hardware.p1: ../../contiki-3.0-xc8/platform/c8v2.1/init-base-hardware.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/init-base-hardware.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/init-base-hardware.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/init-base-hardware.p1  ../../contiki-3.0-xc8/platform/c8v2.1/init-base-hardware.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/init-base-hardware.d ${OBJECTDIR}/_ext/830074946/init-base-hardware.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/init-base-hardware.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/init-base-software.p1: ../../contiki-3.0-xc8/platform/c8v2.1/init-base-software.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/init-base-software.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/init-base-software.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/init-base-software.p1  ../../contiki-3.0-xc8/platform/c8v2.1/init-base-software.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/init-base-software.d ${OBJECTDIR}/_ext/830074946/init-base-software.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/init-base-software.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/init-tcpip.p1: ../../contiki-3.0-xc8/platform/c8v2.1/init-tcpip.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/init-tcpip.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/init-tcpip.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/init-tcpip.p1  ../../contiki-3.0-xc8/platform/c8v2.1/init-tcpip.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/init-tcpip.d ${OBJECTDIR}/_ext/830074946/init-tcpip.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/init-tcpip.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/interrupts.p1: ../../contiki-3.0-xc8/platform/c8v2.1/interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/interrupts.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/interrupts.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/interrupts.p1  ../../contiki-3.0-xc8/platform/c8v2.1/interrupts.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/interrupts.d ${OBJECTDIR}/_ext/830074946/interrupts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/interrupts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/SPIFlash.p1: ../../contiki-3.0-xc8/platform/c8v2.1/SPIFlash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/SPIFlash.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/SPIFlash.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/SPIFlash.p1  ../../contiki-3.0-xc8/platform/c8v2.1/SPIFlash.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/SPIFlash.d ${OBJECTDIR}/_ext/830074946/SPIFlash.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/SPIFlash.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/tcpip-stack-tasks.p1: ../../contiki-3.0-xc8/platform/c8v2.1/tcpip-stack-tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/tcpip-stack-tasks.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/tcpip-stack-tasks.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/tcpip-stack-tasks.p1  ../../contiki-3.0-xc8/platform/c8v2.1/tcpip-stack-tasks.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/tcpip-stack-tasks.d ${OBJECTDIR}/_ext/830074946/tcpip-stack-tasks.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/tcpip-stack-tasks.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/device-conf.p1: ../../contiki-3.0-xc8/platform/c8v2.1/device-conf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/device-conf.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/device-conf.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/device-conf.p1  ../../contiki-3.0-xc8/platform/c8v2.1/device-conf.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/device-conf.d ${OBJECTDIR}/_ext/830074946/device-conf.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/device-conf.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/app-config.p1: ../../contiki-3.0-xc8/platform/c8v2.1/app-config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/app-config.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/app-config.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/app-config.p1  ../../contiki-3.0-xc8/platform/c8v2.1/app-config.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/app-config.d ${OBJECTDIR}/_ext/830074946/app-config.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/app-config.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/sampling.p1: ../../contiki-3.0-xc8/platform/c8v2.1/sampling.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/sampling.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/sampling.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/sampling.p1  ../../contiki-3.0-xc8/platform/c8v2.1/sampling.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/sampling.d ${OBJECTDIR}/_ext/830074946/sampling.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/sampling.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/contiki-main.p1: ../../contiki-3.0-xc8/platform/c8v2.1/contiki-main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/contiki-main.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/contiki-main.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/contiki-main.p1  ../../contiki-3.0-xc8/platform/c8v2.1/contiki-main.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/contiki-main.d ${OBJECTDIR}/_ext/830074946/contiki-main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/contiki-main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/puls-sync.p1: ../../contiki-3.0-xc8/platform/c8v2.1/puls-sync.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/puls-sync.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/puls-sync.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/puls-sync.p1  ../../contiki-3.0-xc8/platform/c8v2.1/puls-sync.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/puls-sync.d ${OBJECTDIR}/_ext/830074946/puls-sync.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/puls-sync.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/CustomHTTPApp.p1: CustomHTTPApp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CustomHTTPApp.p1.d 
	@${RM} ${OBJECTDIR}/CustomHTTPApp.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/CustomHTTPApp.p1  CustomHTTPApp.c 
	@-${MV} ${OBJECTDIR}/CustomHTTPApp.d ${OBJECTDIR}/CustomHTTPApp.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/CustomHTTPApp.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/pulse_counter.p1: pulse_counter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/pulse_counter.p1.d 
	@${RM} ${OBJECTDIR}/pulse_counter.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,-asm,-asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DAUTOSTART_ENABLE=1 -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/pulse_counter.p1  pulse_counter.c 
	@-${MV} ${OBJECTDIR}/pulse_counter.d ${OBJECTDIR}/pulse_counter.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/pulse_counter.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/_ext/1520330545/strncasecmp.p1: ../../contiki-3.0-xc8/core/lib/strncasecmp/strncasecmp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1520330545" 
	@${RM} ${OBJECTDIR}/_ext/1520330545/strncasecmp.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1520330545/strncasecmp.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1520330545/strncasecmp.p1  ../../contiki-3.0-xc8/core/lib/strncasecmp/strncasecmp.c 
	@-${MV} ${OBJECTDIR}/_ext/1520330545/strncasecmp.d ${OBJECTDIR}/_ext/1520330545/strncasecmp.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1520330545/strncasecmp.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/aes-128.p1: ../../contiki-3.0-xc8/core/lib/aes-128.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/aes-128.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/aes-128.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/aes-128.p1  ../../contiki-3.0-xc8/core/lib/aes-128.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/aes-128.d ${OBJECTDIR}/_ext/1460547545/aes-128.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/aes-128.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/assert.p1: ../../contiki-3.0-xc8/core/lib/assert.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/assert.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/assert.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/assert.p1  ../../contiki-3.0-xc8/core/lib/assert.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/assert.d ${OBJECTDIR}/_ext/1460547545/assert.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/assert.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/ccm-star.p1: ../../contiki-3.0-xc8/core/lib/ccm-star.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/ccm-star.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/ccm-star.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/ccm-star.p1  ../../contiki-3.0-xc8/core/lib/ccm-star.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/ccm-star.d ${OBJECTDIR}/_ext/1460547545/ccm-star.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/ccm-star.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/crc16.p1: ../../contiki-3.0-xc8/core/lib/crc16.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/crc16.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/crc16.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/crc16.p1  ../../contiki-3.0-xc8/core/lib/crc16.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/crc16.d ${OBJECTDIR}/_ext/1460547545/crc16.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/crc16.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/gcr.p1: ../../contiki-3.0-xc8/core/lib/gcr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/gcr.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/gcr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/gcr.p1  ../../contiki-3.0-xc8/core/lib/gcr.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/gcr.d ${OBJECTDIR}/_ext/1460547545/gcr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/gcr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/ifft.p1: ../../contiki-3.0-xc8/core/lib/ifft.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/ifft.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/ifft.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/ifft.p1  ../../contiki-3.0-xc8/core/lib/ifft.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/ifft.d ${OBJECTDIR}/_ext/1460547545/ifft.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/ifft.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/list.p1: ../../contiki-3.0-xc8/core/lib/list.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/list.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/list.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/list.p1  ../../contiki-3.0-xc8/core/lib/list.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/list.d ${OBJECTDIR}/_ext/1460547545/list.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/list.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/me.p1: ../../contiki-3.0-xc8/core/lib/me.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/me.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/me.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/me.p1  ../../contiki-3.0-xc8/core/lib/me.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/me.d ${OBJECTDIR}/_ext/1460547545/me.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/me.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/memb.p1: ../../contiki-3.0-xc8/core/lib/memb.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/memb.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/memb.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/memb.p1  ../../contiki-3.0-xc8/core/lib/memb.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/memb.d ${OBJECTDIR}/_ext/1460547545/memb.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/memb.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/me_tabs.p1: ../../contiki-3.0-xc8/core/lib/me_tabs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/me_tabs.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/me_tabs.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/me_tabs.p1  ../../contiki-3.0-xc8/core/lib/me_tabs.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/me_tabs.d ${OBJECTDIR}/_ext/1460547545/me_tabs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/me_tabs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/mmem.p1: ../../contiki-3.0-xc8/core/lib/mmem.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/mmem.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/mmem.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/mmem.p1  ../../contiki-3.0-xc8/core/lib/mmem.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/mmem.d ${OBJECTDIR}/_ext/1460547545/mmem.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/mmem.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/petsciiconv.p1: ../../contiki-3.0-xc8/core/lib/petsciiconv.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/petsciiconv.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/petsciiconv.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/petsciiconv.p1  ../../contiki-3.0-xc8/core/lib/petsciiconv.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/petsciiconv.d ${OBJECTDIR}/_ext/1460547545/petsciiconv.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/petsciiconv.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/random.p1: ../../contiki-3.0-xc8/core/lib/random.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/random.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/random.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/random.p1  ../../contiki-3.0-xc8/core/lib/random.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/random.d ${OBJECTDIR}/_ext/1460547545/random.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/random.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/ringbuf.p1: ../../contiki-3.0-xc8/core/lib/ringbuf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/ringbuf.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/ringbuf.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/ringbuf.p1  ../../contiki-3.0-xc8/core/lib/ringbuf.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/ringbuf.d ${OBJECTDIR}/_ext/1460547545/ringbuf.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/ringbuf.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460547545/settings.p1: ../../contiki-3.0-xc8/core/lib/settings.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460547545" 
	@${RM} ${OBJECTDIR}/_ext/1460547545/settings.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460547545/settings.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460547545/settings.p1  ../../contiki-3.0-xc8/core/lib/settings.c 
	@-${MV} ${OBJECTDIR}/_ext/1460547545/settings.d ${OBJECTDIR}/_ext/1460547545/settings.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460547545/settings.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/arg.p1: ../../contiki-3.0-xc8/core/sys/arg.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/arg.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/arg.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/arg.p1  ../../contiki-3.0-xc8/core/sys/arg.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/arg.d ${OBJECTDIR}/_ext/1460540305/arg.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/arg.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/autostart.p1: ../../contiki-3.0-xc8/core/sys/autostart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/autostart.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/autostart.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/autostart.p1  ../../contiki-3.0-xc8/core/sys/autostart.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/autostart.d ${OBJECTDIR}/_ext/1460540305/autostart.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/autostart.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/ctimer.p1: ../../contiki-3.0-xc8/core/sys/ctimer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/ctimer.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/ctimer.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/ctimer.p1  ../../contiki-3.0-xc8/core/sys/ctimer.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/ctimer.d ${OBJECTDIR}/_ext/1460540305/ctimer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/ctimer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/energest.p1: ../../contiki-3.0-xc8/core/sys/energest.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/energest.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/energest.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/energest.p1  ../../contiki-3.0-xc8/core/sys/energest.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/energest.d ${OBJECTDIR}/_ext/1460540305/energest.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/energest.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/etimer.p1: ../../contiki-3.0-xc8/core/sys/etimer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/etimer.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/etimer.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/etimer.p1  ../../contiki-3.0-xc8/core/sys/etimer.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/etimer.d ${OBJECTDIR}/_ext/1460540305/etimer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/etimer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/process.p1: ../../contiki-3.0-xc8/core/sys/process.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/process.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/process.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/process.p1  ../../contiki-3.0-xc8/core/sys/process.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/process.d ${OBJECTDIR}/_ext/1460540305/process.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/process.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/procinit.p1: ../../contiki-3.0-xc8/core/sys/procinit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/procinit.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/procinit.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/procinit.p1  ../../contiki-3.0-xc8/core/sys/procinit.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/procinit.d ${OBJECTDIR}/_ext/1460540305/procinit.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/procinit.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/rtimer.p1: ../../contiki-3.0-xc8/core/sys/rtimer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/rtimer.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/rtimer.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/rtimer.p1  ../../contiki-3.0-xc8/core/sys/rtimer.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/rtimer.d ${OBJECTDIR}/_ext/1460540305/rtimer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/rtimer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/stimer.p1: ../../contiki-3.0-xc8/core/sys/stimer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/stimer.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/stimer.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/stimer.p1  ../../contiki-3.0-xc8/core/sys/stimer.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/stimer.d ${OBJECTDIR}/_ext/1460540305/stimer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/stimer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1460540305/timer.p1: ../../contiki-3.0-xc8/core/sys/timer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1460540305" 
	@${RM} ${OBJECTDIR}/_ext/1460540305/timer.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1460540305/timer.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1460540305/timer.p1  ../../contiki-3.0-xc8/core/sys/timer.c 
	@-${MV} ${OBJECTDIR}/_ext/1460540305/timer.d ${OBJECTDIR}/_ext/1460540305/timer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1460540305/timer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/DNSs.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DNSs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/DNSs.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/DNSs.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/DNSs.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DNSs.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/DNSs.d ${OBJECTDIR}/_ext/778749567/DNSs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/DNSs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/MPFS2.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/MPFS2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/MPFS2.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/MPFS2.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/MPFS2.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/MPFS2.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/MPFS2.d ${OBJECTDIR}/_ext/778749567/MPFS2.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/MPFS2.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/Announce.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Announce.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/Announce.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/Announce.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/Announce.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Announce.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/Announce.d ${OBJECTDIR}/_ext/778749567/Announce.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/Announce.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/ARCFOUR.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ARCFOUR.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/ARCFOUR.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/ARCFOUR.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/ARCFOUR.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ARCFOUR.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/ARCFOUR.d ${OBJECTDIR}/_ext/778749567/ARCFOUR.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/ARCFOUR.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/ARP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ARP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/ARP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/ARP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/ARP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ARP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/ARP.d ${OBJECTDIR}/_ext/778749567/ARP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/ARP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/AutoIP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/AutoIP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/AutoIP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/AutoIP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/AutoIP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/AutoIP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/AutoIP.d ${OBJECTDIR}/_ext/778749567/AutoIP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/AutoIP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/BerkeleyAPI.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/BerkeleyAPI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/BerkeleyAPI.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/BerkeleyAPI.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/BerkeleyAPI.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/BerkeleyAPI.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/BerkeleyAPI.d ${OBJECTDIR}/_ext/778749567/BerkeleyAPI.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/BerkeleyAPI.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/BigInt.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/BigInt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/BigInt.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/BigInt.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/BigInt.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/BigInt.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/BigInt.d ${OBJECTDIR}/_ext/778749567/BigInt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/BigInt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/Delay.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/Delay.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/Delay.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/Delay.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Delay.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/Delay.d ${OBJECTDIR}/_ext/778749567/Delay.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/Delay.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/DHCP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DHCP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/DHCP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/DHCP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/DHCP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DHCP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/DHCP.d ${OBJECTDIR}/_ext/778749567/DHCP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/DHCP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/DHCPs.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DHCPs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/DHCPs.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/DHCPs.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/DHCPs.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DHCPs.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/DHCPs.d ${OBJECTDIR}/_ext/778749567/DHCPs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/DHCPs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/DNS.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DNS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/DNS.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/DNS.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/DNS.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DNS.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/DNS.d ${OBJECTDIR}/_ext/778749567/DNS.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/DNS.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/DynDNS.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DynDNS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/DynDNS.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/DynDNS.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/DynDNS.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/DynDNS.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/DynDNS.d ${OBJECTDIR}/_ext/778749567/DynDNS.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/DynDNS.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/ENC28J60.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ENC28J60.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/ENC28J60.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/ENC28J60.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/ENC28J60.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ENC28J60.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/ENC28J60.d ${OBJECTDIR}/_ext/778749567/ENC28J60.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/ENC28J60.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/ENCX24J600.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ENCX24J600.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/ENCX24J600.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/ENCX24J600.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/ENCX24J600.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ENCX24J600.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/ENCX24J600.d ${OBJECTDIR}/_ext/778749567/ENCX24J600.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/ENCX24J600.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/ETH97J60.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ETH97J60.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/ETH97J60.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/ETH97J60.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/ETH97J60.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ETH97J60.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/ETH97J60.d ${OBJECTDIR}/_ext/778749567/ETH97J60.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/ETH97J60.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/FileSystem.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/FileSystem.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/FileSystem.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/FileSystem.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/FileSystem.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/FileSystem.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/FileSystem.d ${OBJECTDIR}/_ext/778749567/FileSystem.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/FileSystem.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/FTP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/FTP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/FTP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/FTP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/FTP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/FTP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/FTP.d ${OBJECTDIR}/_ext/778749567/FTP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/FTP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/Hashes.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Hashes.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/Hashes.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/Hashes.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/Hashes.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Hashes.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/Hashes.d ${OBJECTDIR}/_ext/778749567/Hashes.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/Hashes.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/Helpers.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Helpers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/Helpers.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/Helpers.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/Helpers.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Helpers.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/Helpers.d ${OBJECTDIR}/_ext/778749567/Helpers.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/Helpers.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/HTTP2.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/HTTP2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/HTTP2.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/HTTP2.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/HTTP2.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/HTTP2.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/HTTP2.d ${OBJECTDIR}/_ext/778749567/HTTP2.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/HTTP2.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/ICMP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ICMP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/ICMP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/ICMP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/ICMP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/ICMP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/ICMP.d ${OBJECTDIR}/_ext/778749567/ICMP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/ICMP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/IP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/IP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/IP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/IP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/IP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/IP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/IP.d ${OBJECTDIR}/_ext/778749567/IP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/IP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/LCDBlocking.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/LCDBlocking.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/LCDBlocking.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/LCDBlocking.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/LCDBlocking.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/LCDBlocking.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/LCDBlocking.d ${OBJECTDIR}/_ext/778749567/LCDBlocking.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/LCDBlocking.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/NBNS.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/NBNS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/NBNS.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/NBNS.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/NBNS.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/NBNS.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/NBNS.d ${OBJECTDIR}/_ext/778749567/NBNS.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/NBNS.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/Random.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Random.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/Random.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/Random.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/Random.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Random.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/Random.d ${OBJECTDIR}/_ext/778749567/Random.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/Random.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/Reboot.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Reboot.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/Reboot.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/Reboot.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/Reboot.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Reboot.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/Reboot.d ${OBJECTDIR}/_ext/778749567/Reboot.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/Reboot.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/RSA.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/RSA.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/RSA.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/RSA.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/RSA.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/RSA.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/RSA.d ${OBJECTDIR}/_ext/778749567/RSA.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/RSA.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/SMTP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SMTP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/SMTP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/SMTP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/SMTP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SMTP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/SMTP.d ${OBJECTDIR}/_ext/778749567/SMTP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/SMTP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/SNMP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SNMP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/SNMP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/SNMP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/SNMP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SNMP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/SNMP.d ${OBJECTDIR}/_ext/778749567/SNMP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/SNMP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/SNTP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SNTP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/SNTP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/SNTP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/SNTP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SNTP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/SNTP.d ${OBJECTDIR}/_ext/778749567/SNTP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/SNTP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/SPIEEPROM.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SPIEEPROM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/SPIEEPROM.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/SPIEEPROM.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/SPIEEPROM.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SPIEEPROM.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/SPIEEPROM.d ${OBJECTDIR}/_ext/778749567/SPIEEPROM.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/SPIEEPROM.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/SPIRAM.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SPIRAM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/SPIRAM.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/SPIRAM.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/SPIRAM.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SPIRAM.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/SPIRAM.d ${OBJECTDIR}/_ext/778749567/SPIRAM.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/SPIRAM.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/SSL.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SSL.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/SSL.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/SSL.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/SSL.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/SSL.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/SSL.d ${OBJECTDIR}/_ext/778749567/SSL.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/SSL.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/StackTsk.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/StackTsk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/StackTsk.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/StackTsk.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/StackTsk.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/StackTsk.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/StackTsk.d ${OBJECTDIR}/_ext/778749567/StackTsk.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/StackTsk.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/TCP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/TCP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/TCP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/TCP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/TCP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/TCP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/TCP.d ${OBJECTDIR}/_ext/778749567/TCP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/TCP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/TCPPerformanceTest.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/TCPPerformanceTest.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/TCPPerformanceTest.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/TCPPerformanceTest.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/TCPPerformanceTest.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/TCPPerformanceTest.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/TCPPerformanceTest.d ${OBJECTDIR}/_ext/778749567/TCPPerformanceTest.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/TCPPerformanceTest.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/Telnet.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Telnet.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/Telnet.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/Telnet.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/Telnet.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Telnet.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/Telnet.d ${OBJECTDIR}/_ext/778749567/Telnet.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/Telnet.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/TFTPc.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/TFTPc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/TFTPc.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/TFTPc.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/TFTPc.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/TFTPc.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/TFTPc.d ${OBJECTDIR}/_ext/778749567/TFTPc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/TFTPc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/Tick.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Tick.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/Tick.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/Tick.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/Tick.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/Tick.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/Tick.d ${OBJECTDIR}/_ext/778749567/Tick.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/Tick.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/UART.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UART.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/UART.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/UART.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/UART.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UART.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/UART.d ${OBJECTDIR}/_ext/778749567/UART.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/UART.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/UART2TCPBridge.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UART2TCPBridge.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/UART2TCPBridge.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/UART2TCPBridge.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/UART2TCPBridge.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UART2TCPBridge.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/UART2TCPBridge.d ${OBJECTDIR}/_ext/778749567/UART2TCPBridge.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/UART2TCPBridge.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/UDP.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UDP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/UDP.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/UDP.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/UDP.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UDP.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/UDP.d ${OBJECTDIR}/_ext/778749567/UDP.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/UDP.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/778749567/UDPPerformanceTest.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UDPPerformanceTest.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/778749567" 
	@${RM} ${OBJECTDIR}/_ext/778749567/UDPPerformanceTest.p1.d 
	@${RM} ${OBJECTDIR}/_ext/778749567/UDPPerformanceTest.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/778749567/UDPPerformanceTest.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/TCPIPStack/UDPPerformanceTest.c 
	@-${MV} ${OBJECTDIR}/_ext/778749567/UDPPerformanceTest.d ${OBJECTDIR}/_ext/778749567/UDPPerformanceTest.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/778749567/UDPPerformanceTest.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/518696466/microchip-tcpip-stack.p1: ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/microchip-tcpip-stack.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/518696466" 
	@${RM} ${OBJECTDIR}/_ext/518696466/microchip-tcpip-stack.p1.d 
	@${RM} ${OBJECTDIR}/_ext/518696466/microchip-tcpip-stack.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/518696466/microchip-tcpip-stack.p1  ../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/microchip-tcpip-stack.c 
	@-${MV} ${OBJECTDIR}/_ext/518696466/microchip-tcpip-stack.d ${OBJECTDIR}/_ext/518696466/microchip-tcpip-stack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/518696466/microchip-tcpip-stack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/109911484/ports-adc.p1: ../../contiki-3.0-xc8/platform/c8v2.1/dev/ports-adc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/109911484" 
	@${RM} ${OBJECTDIR}/_ext/109911484/ports-adc.p1.d 
	@${RM} ${OBJECTDIR}/_ext/109911484/ports-adc.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/109911484/ports-adc.p1  ../../contiki-3.0-xc8/platform/c8v2.1/dev/ports-adc.c 
	@-${MV} ${OBJECTDIR}/_ext/109911484/ports-adc.d ${OBJECTDIR}/_ext/109911484/ports-adc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/109911484/ports-adc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/109911484/ports-leds.p1: ../../contiki-3.0-xc8/platform/c8v2.1/dev/ports-leds.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/109911484" 
	@${RM} ${OBJECTDIR}/_ext/109911484/ports-leds.p1.d 
	@${RM} ${OBJECTDIR}/_ext/109911484/ports-leds.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/109911484/ports-leds.p1  ../../contiki-3.0-xc8/platform/c8v2.1/dev/ports-leds.c 
	@-${MV} ${OBJECTDIR}/_ext/109911484/ports-leds.d ${OBJECTDIR}/_ext/109911484/ports-leds.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/109911484/ports-leds.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/109911484/ports-shifter.p1: ../../contiki-3.0-xc8/platform/c8v2.1/dev/ports-shifter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/109911484" 
	@${RM} ${OBJECTDIR}/_ext/109911484/ports-shifter.p1.d 
	@${RM} ${OBJECTDIR}/_ext/109911484/ports-shifter.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/109911484/ports-shifter.p1  ../../contiki-3.0-xc8/platform/c8v2.1/dev/ports-shifter.c 
	@-${MV} ${OBJECTDIR}/_ext/109911484/ports-shifter.d ${OBJECTDIR}/_ext/109911484/ports-shifter.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/109911484/ports-shifter.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/clock.p1: ../../contiki-3.0-xc8/platform/c8v2.1/clock.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/clock.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/clock.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/clock.p1  ../../contiki-3.0-xc8/platform/c8v2.1/clock.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/clock.d ${OBJECTDIR}/_ext/830074946/clock.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/clock.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/configuration-bits.p1: ../../contiki-3.0-xc8/platform/c8v2.1/configuration-bits.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/configuration-bits.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/configuration-bits.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/configuration-bits.p1  ../../contiki-3.0-xc8/platform/c8v2.1/configuration-bits.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/configuration-bits.d ${OBJECTDIR}/_ext/830074946/configuration-bits.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/configuration-bits.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/init-base-hardware.p1: ../../contiki-3.0-xc8/platform/c8v2.1/init-base-hardware.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/init-base-hardware.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/init-base-hardware.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/init-base-hardware.p1  ../../contiki-3.0-xc8/platform/c8v2.1/init-base-hardware.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/init-base-hardware.d ${OBJECTDIR}/_ext/830074946/init-base-hardware.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/init-base-hardware.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/init-base-software.p1: ../../contiki-3.0-xc8/platform/c8v2.1/init-base-software.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/init-base-software.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/init-base-software.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/init-base-software.p1  ../../contiki-3.0-xc8/platform/c8v2.1/init-base-software.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/init-base-software.d ${OBJECTDIR}/_ext/830074946/init-base-software.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/init-base-software.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/init-tcpip.p1: ../../contiki-3.0-xc8/platform/c8v2.1/init-tcpip.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/init-tcpip.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/init-tcpip.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/init-tcpip.p1  ../../contiki-3.0-xc8/platform/c8v2.1/init-tcpip.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/init-tcpip.d ${OBJECTDIR}/_ext/830074946/init-tcpip.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/init-tcpip.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/interrupts.p1: ../../contiki-3.0-xc8/platform/c8v2.1/interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/interrupts.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/interrupts.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/interrupts.p1  ../../contiki-3.0-xc8/platform/c8v2.1/interrupts.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/interrupts.d ${OBJECTDIR}/_ext/830074946/interrupts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/interrupts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/SPIFlash.p1: ../../contiki-3.0-xc8/platform/c8v2.1/SPIFlash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/SPIFlash.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/SPIFlash.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/SPIFlash.p1  ../../contiki-3.0-xc8/platform/c8v2.1/SPIFlash.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/SPIFlash.d ${OBJECTDIR}/_ext/830074946/SPIFlash.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/SPIFlash.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/tcpip-stack-tasks.p1: ../../contiki-3.0-xc8/platform/c8v2.1/tcpip-stack-tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/tcpip-stack-tasks.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/tcpip-stack-tasks.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/tcpip-stack-tasks.p1  ../../contiki-3.0-xc8/platform/c8v2.1/tcpip-stack-tasks.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/tcpip-stack-tasks.d ${OBJECTDIR}/_ext/830074946/tcpip-stack-tasks.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/tcpip-stack-tasks.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/device-conf.p1: ../../contiki-3.0-xc8/platform/c8v2.1/device-conf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/device-conf.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/device-conf.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/device-conf.p1  ../../contiki-3.0-xc8/platform/c8v2.1/device-conf.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/device-conf.d ${OBJECTDIR}/_ext/830074946/device-conf.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/device-conf.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/app-config.p1: ../../contiki-3.0-xc8/platform/c8v2.1/app-config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/app-config.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/app-config.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/app-config.p1  ../../contiki-3.0-xc8/platform/c8v2.1/app-config.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/app-config.d ${OBJECTDIR}/_ext/830074946/app-config.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/app-config.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/sampling.p1: ../../contiki-3.0-xc8/platform/c8v2.1/sampling.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/sampling.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/sampling.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/sampling.p1  ../../contiki-3.0-xc8/platform/c8v2.1/sampling.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/sampling.d ${OBJECTDIR}/_ext/830074946/sampling.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/sampling.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/contiki-main.p1: ../../contiki-3.0-xc8/platform/c8v2.1/contiki-main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/contiki-main.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/contiki-main.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/contiki-main.p1  ../../contiki-3.0-xc8/platform/c8v2.1/contiki-main.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/contiki-main.d ${OBJECTDIR}/_ext/830074946/contiki-main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/contiki-main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/830074946/puls-sync.p1: ../../contiki-3.0-xc8/platform/c8v2.1/puls-sync.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/830074946" 
	@${RM} ${OBJECTDIR}/_ext/830074946/puls-sync.p1.d 
	@${RM} ${OBJECTDIR}/_ext/830074946/puls-sync.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/830074946/puls-sync.p1  ../../contiki-3.0-xc8/platform/c8v2.1/puls-sync.c 
	@-${MV} ${OBJECTDIR}/_ext/830074946/puls-sync.d ${OBJECTDIR}/_ext/830074946/puls-sync.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/830074946/puls-sync.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/CustomHTTPApp.p1: CustomHTTPApp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CustomHTTPApp.p1.d 
	@${RM} ${OBJECTDIR}/CustomHTTPApp.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/CustomHTTPApp.p1  CustomHTTPApp.c 
	@-${MV} ${OBJECTDIR}/CustomHTTPApp.d ${OBJECTDIR}/CustomHTTPApp.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/CustomHTTPApp.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/pulse_counter.p1: pulse_counter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/pulse_counter.p1.d 
	@${RM} ${OBJECTDIR}/pulse_counter.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,-asm,-asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DAUTOSTART_ENABLE=1 -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/pulse_counter.p1  pulse_counter.c 
	@-${MV} ${OBJECTDIR}/pulse_counter.d ${OBJECTDIR}/pulse_counter.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/pulse_counter.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/c8v2.1-spiralki.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -mdist/${CND_CONF}/${IMAGE_TYPE}/c8v2.1-spiralki.X.${IMAGE_TYPE}.map  -D__DEBUG=1 --debugger=realice  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"       --memorysummary dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -odist/${CND_CONF}/${IMAGE_TYPE}/c8v2.1-spiralki.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/c8v2.1-spiralki.X.${IMAGE_TYPE}.hex 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/c8v2.1-spiralki.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -mdist/${CND_CONF}/${IMAGE_TYPE}/c8v2.1-spiralki.X.${IMAGE_TYPE}.map  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,-asmfile,+speed,-space,-debug --addrqual=ignore --mode=pro -P -N255 -I"." -I"../../contiki-3.0-xc8/platform/c8v2.1" -I"../../contiki-3.0-xc8/cpu/PIC18/MAPLv2013-06-15/Microchip/Include" -I"../../contiki-3.0-xc8/core" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=+mcof,-elf:multilocs --stack=reentrant:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    --memorysummary dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -odist/${CND_CONF}/${IMAGE_TYPE}/c8v2.1-spiralki.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
