#include "contiki.h"
#include "pulse-counter.h"
#include "sampling.h"
#include "dev/ports-leds.h"
#include "dev/ports-shifter.h"
#include <string.h>

static struct PulseCounters counters[PULSE_COUNTER_COUNTERS_NBR] = {0};

static void probes_init(void);
static void probes_input(uint8_t channel, bool value);

PROCESS(pulse_counter, "Licznik impulsow");

AUTOSTART_PROCESSES(
        AUTOSTART_ENTRY(&pulse_counter)
        );

PROCESS_THREAD(pulse_counter, ev, data) {
    static uint8_t i;
    PROCESS_BEGIN();
    pulse_counter_reset();
    ports_shifter_init();
    ports_shifter_set(0xfffful);
    while (1) {
        PROCESS_WAIT_EVENT_UNTIL(ev == sampling_event);
        while (sampling_writing_pointer != sampling_reading_pointer) {
            uint8_t diff_mask;
            diff_mask = sampling_output_buffer[sampling_reading_pointer]^sampling_output_buffer[(uint8_t) (sampling_reading_pointer - 1)];
            if (diff_mask) {
                for (i = 0; diff_mask; diff_mask >>= 1u, i++) {
                    if (diff_mask & 0b1u) {
                        probes_input(i, sampling_output_buffer[sampling_reading_pointer]&(1u << i));
                    }
                }
            }
            sampling_reading_pointer++;
        }
    }
    PROCESS_END();
}

void pulse_counter_reset(void) {
    memset(counters, 0, sizeof (counters));
    //process_post(&sampling_process, PROCESS_EVENT_INIT, NULL);
    sampling_register_observer(&pulse_counter);

}

static void probes_input(uint8_t channel, bool value) {
    static uint8_t prev_states_mask = 0;
    static uint8_t stable_states_mask = 0;

    if (channel >= PULSE_COUNTER_COUNTERS_NBR) return;
    value = !!value;
    if (value == !!(prev_states_mask & (1u << channel))) {
        stable_states_mask &= ~(1u << channel);
        stable_states_mask |= (value << channel);
        //ports_leds_flash_once(channel, value ? PORTS_LEDS_COLOR_RED : PORTS_LEDS_COLOR_GREEN);
        //ports_leds_blink(channel, value ? PORTS_LEDS_COLOR_RED : PORTS_LEDS_COLOR_GREEN);
        //ports_leds_off(channel, value ? PORTS_LEDS_COLOR_GREEN : PORTS_LEDS_COLOR_RED);
        //process_poll(&ports_leds_process);
    }
    prev_states_mask &= ~(1u << channel);
    prev_states_mask |= (value << channel);

}

